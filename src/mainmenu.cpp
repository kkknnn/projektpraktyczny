#include "../inc/mainmenu.h"
#include "../inc/viewfactory.h"
#include "../inc/globalvariables.h"
#include <QtFontDatabaseSupport/QtFontDatabaseSupport>
#include <QMessageBox>
#include <QIODevice>
#include <QLabel>
MainMenu::MainMenu(QWidget * parent): QGraphicsView (parent)
{

QBrush Brush;
Brush.setColor(QColor(0,0,255,20));
Brush.setStyle(Qt::SolidPattern);
setBackgroundBrush(Brush);
System=std::make_shared<SysFuction>();
MainMenuScene=std::make_shared<QGraphicsScene>(new QGraphicsScene());
MainMenuScene->setSceneRect(0,0,800,600);
TopPlayers=System->ReadBestPlayers(QString("/home/iks/ProjektPraktyczny/data/bestplayers.dat"));
setScene(MainMenuScene.get());
setFixedSize(static_cast<int>(MainMenuScene->width()),static_cast<int>(MainMenuScene->height()));
setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
int newfont = QFontDatabase::addApplicationFont(":/fonts/fonts/Retronoid.ttf");
QString ButtonFont = QFontDatabase::applicationFontFamilies(newfont).at(0);
QFont buttonFont(ButtonFont);
buttonFont.setPointSize(35);
int newfont2 = QFontDatabase::addApplicationFont(":/fonts/fonts/Clone Machine.otf");
QString TextFont = QFontDatabase::applicationFontFamilies(newfont2).at(0);
QFont textFont(TextFont);
textFont.setBold(true);
NewGameButton=std::make_shared<QPushButton>(new QPushButton());
NewGameButton->setGeometry(250,100,300,50);
NewGameButton->setText("NEW GAME");
NewGameButton->setFont(buttonFont);
NewGameButton->setFlat(true);
NewGameButton->setParent(nullptr);
NewGameButton->setStyleSheet("QPushButton {background-color: #A3C1DA; color: white;}");
MainMenuScene->addWidget(NewGameButton.get());
QuitGameButton=std::make_shared<QPushButton>();
QuitGameButton->setGeometry(250,500,300,50);
QuitGameButton->setText("QUIT");
QuitGameButton->setFont(buttonFont);
QuitGameButton->setStyleSheet("QPushButton {background-color: red; color: white;}");
QuitGameButton->setFlat(true);
QuitGameButton->setParent(nullptr);
MainMenuScene->addWidget(QuitGameButton.get());
PlayerName = std::make_shared<QLineEdit>();
PlayerName->setGeometry(250,180,300,30);
PlayerName->setFont(textFont);
PlayerName->setAlignment(Qt::AlignCenter);
PlayerName->setText(QString("Enter your nickname"));
PlayerName->setParent(nullptr);
MainMenuScene->addWidget(PlayerName.get());
BestPlayerLabel = std::make_shared<QLabel>();
BestPlayerLabel->setGeometry(250,250,300,200);
BestPlayerLabel->setAlignment(Qt::AlignHCenter);
BestPlayerLabel->setFont(textFont);
BestPlayerLabel->setText(QString("BEST PLAYERS"));
BestPlayerLabel->setParent(nullptr);
QString BestPlayerList = "BEST PLAYERS";
BestPlayerList.append('\n');
for (int var = 0; var < TopPlayers.size(); ++var)
{

    QString temp = TopPlayers[var].first + " " +QString::number(TopPlayers[var].second);
    BestPlayerList.append(temp);
    BestPlayerList.append('\n');

}
BestPlayerLabel->setText(BestPlayerList);
MainMenuScene->addWidget(BestPlayerLabel.get());
PacmanImage = std::make_shared<QGraphicsPixmapItem>();
PacmanImage->setPixmap(QPixmap(":/img/img/pacbig.png"));
PacmanImage->setScale(0.8);
PacmanImage->setPos(QPoint(0,20));
GhostImage = std::make_shared<QGraphicsPixmapItem>();
GhostImage->setPixmap(QPixmap(":/img/img/ghostbig.png"));
GhostImage->setPos(600,400);
GhostImage->setScale(0.8);

MainMenuScene->addItem(PacmanImage.get());
MainMenuScene->addItem(GhostImage.get());
connect(NewGameButton.get(),SIGNAL(released()),this, SLOT(GameInit()));
connect(QuitGameButton.get(),SIGNAL(released()),this, SLOT(close()));
System->showView(this);
}

MainMenu::MainMenu(SysFuctionInterface *Sys, QWidget *parent): QGraphicsView (parent)
{
    QBrush Brush;
    Brush.setColor(QColor(0,0,255,20));
    Brush.setStyle(Qt::SolidPattern);
    setBackgroundBrush(Brush);
    System.reset(Sys);
    MainMenuScene=std::make_shared<QGraphicsScene>(new QGraphicsScene());
    MainMenuScene->setSceneRect(0,0,800,600);
    TopPlayers=System->ReadBestPlayers(QString("/home/iks/ProjektPraktyczny/data/bestplayers.dat"));
    setScene(MainMenuScene.get());
    setFixedSize(static_cast<int>(MainMenuScene->width()),static_cast<int>(MainMenuScene->height()));
    setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    int newfont = QFontDatabase::addApplicationFont(":/fonts/fonts/Retronoid.ttf");
    QString ButtonFont = QFontDatabase::applicationFontFamilies(newfont).at(0);
    QFont buttonFont(ButtonFont);
    buttonFont.setPointSize(35);
    int newfont2 = QFontDatabase::addApplicationFont(":/fonts/fonts/Clone Machine.otf");
    QString TextFont = QFontDatabase::applicationFontFamilies(newfont2).at(0);
    QFont textFont(TextFont);
    textFont.setBold(true);
    NewGameButton=std::make_shared<QPushButton>(new QPushButton());
    NewGameButton->setGeometry(250,100,300,50);
    NewGameButton->setText("NEW GAME");
    NewGameButton->setFont(buttonFont);
    NewGameButton->setFlat(true);
    NewGameButton->setParent(nullptr);
    NewGameButton->setStyleSheet("QPushButton {background-color: #A3C1DA; color: white;}");
    MainMenuScene->addWidget(NewGameButton.get());
    QuitGameButton=std::make_shared<QPushButton>();
    QuitGameButton->setGeometry(250,500,300,50);
    QuitGameButton->setText("QUIT");
    QuitGameButton->setFont(buttonFont);
    QuitGameButton->setStyleSheet("QPushButton {background-color: red; color: white;}");
    QuitGameButton->setFlat(true);
    QuitGameButton->setParent(nullptr);
    MainMenuScene->addWidget(QuitGameButton.get());
    PlayerName = std::make_shared<QLineEdit>();
    PlayerName->setGeometry(250,180,300,30);
    PlayerName->setFont(textFont);
    PlayerName->setAlignment(Qt::AlignCenter);
    PlayerName->setText(QString("Enter your nickname"));
    PlayerName->setParent(nullptr);
    MainMenuScene->addWidget(PlayerName.get());
    BestPlayerLabel = std::make_shared<QLabel>();
    BestPlayerLabel->setGeometry(250,250,300,200);
    BestPlayerLabel->setAlignment(Qt::AlignHCenter);
    BestPlayerLabel->setFont(textFont);
    BestPlayerLabel->setText(QString("BEST PLAYERS"));
    BestPlayerLabel->setParent(nullptr);
    QString BestPlayerList = "BEST PLAYERS";
    BestPlayerList.append('\n');
    for (int var = 0; var < TopPlayers.size(); ++var)
    {

        QString temp = TopPlayers[var].first + " " +QString::number(TopPlayers[var].second);
        BestPlayerList.append(temp);
        BestPlayerList.append('\n');

    }
    BestPlayerLabel->setText(BestPlayerList);
    MainMenuScene->addWidget(BestPlayerLabel.get());
    PacmanImage = std::make_shared<QGraphicsPixmapItem>();
    PacmanImage->setPixmap(QPixmap(":/img/img/pacbig.png"));
    PacmanImage->setScale(0.8);
    PacmanImage->setPos(QPoint(0,20));
    GhostImage = std::make_shared<QGraphicsPixmapItem>();
    GhostImage->setPixmap(QPixmap(":/img/img/ghostbig.png"));
    GhostImage->setPos(600,400);
    GhostImage->setScale(0.8);

    MainMenuScene->addItem(PacmanImage.get());
    MainMenuScene->addItem(GhostImage.get());






    connect(NewGameButton.get(),SIGNAL(released()),this, SLOT(GameInit()));
    connect(QuitGameButton.get(),SIGNAL(released()),this, SLOT(close()));
    System->showView(this);

}


void MainMenu::GameInit()
{
    PlayerNickname=PlayerName.get()->text();
    this->close();
    System->GameInit();
}

QString MainMenu::getPlayerNickname() const
{
    return PlayerNickname;
}

QList<QPair<QString, int> > MainMenu::getTopPlayers() const
{
    return TopPlayers;
}
