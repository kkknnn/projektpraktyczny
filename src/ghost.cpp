#include "../inc/ghost.h"
#include "../inc/game.h"
#include <QPoint>
#include <QLineF>
#include <QDebug>
#include <iostream>
#include <QRandomGenerator>

ghost::ghost(Game *_game, int ghostnumber)
{

    pacmangame=_game;
    Animationphase=0;
    AnimationHelper=1;
    SpeedMayChange=true;
    PhaseChanger=0;
    GhostType=ghostnumber;
    switch (GhostType) {
    case 0:
        right=QPixmap(":/img/img/ghostright.png");
        up=QPixmap(":/img/img/ghostup.png");
        down=QPixmap(":/img/img/ghostdown.png");
        left=QPixmap(":/img/img/ghostleft.png");
        break;
    case 1:
        right=QPixmap(":/img/img/ghostblueright.png");
        up=QPixmap(":/img/img/ghostblueup.png");
        down=QPixmap(":/img/img/ghostbluedown.png");
        left=QPixmap(":/img/img/ghosbluetleft.png");
        break;
    case 2:
        right=QPixmap(":/img/img/ghostyellowright.png");
        up=QPixmap(":/img/img/ghostyellowup.png");
        down=QPixmap(":/img/img/ghostyellowdown.png");
        left=QPixmap(":/img/img/ghostyellowleft.png");
        break;
    case 3:
        right=QPixmap(":/img/img/ghostredright.png");
        up=QPixmap(":/img/img/ghostredup.png");
        down=QPixmap(":/img/img/ghostreddown.png");
        left=QPixmap(":/img/img/ghostredleft.png");
        break;
    }
    valnurable=QPixmap(":/img/img/ghostvulnerable.png");
    setPixmap(up);
    lastmove=QPoint(216,240);
    setPos(lastmove);
    PowerballMode=false;
    WaitMode = true;
    ScatterMode = false;
    DeadMode = false;
    ChaseMode = false;
    VelocityX=0;
    VelocityY=0;
    Speed=2;
    targetdestination=QPoint(999,999);
    ChaseSettingTarget = new QTimer(this);
    ChaseSettingTarget->setInterval(10000);
    connect(ChaseSettingTarget,SIGNAL(timeout()),this, SLOT(setChaseModeTarget()));

}

void ghost::update()
{
    if (DeadMode && SpeedMayChange)
    {
        Speed=8;
        SpeedMayChange=false;
        setPos(nextmove);
    }

 QPoint actualposition = QPoint(int(this->pos().x()),int(pos().y()));

    if(pacmangame->board->checkIfCrossroad(actualposition))
    {
        ChooseNextMove();
        setVelocity(actualposition);
        lastmove=actualposition;
        if (actualposition==QPoint(216,240))
        {
            if (DeadMode)
            {
                DeadMode=false;
                WaitMode=true;
                SpeedMayChange=true;

            }

            Speed=2;

        }
        animate();
    }

    if (pacmangame->isPowerBallMode())
        {
            setPixmap(valnurable);
        }

    if(!WaitMode)
    {

    move();
    }
}

void ghost::setScatteredModeTarget()
{

    if (ScatterMode==true)
    {
    QRandomGenerator generator;
    int x = static_cast<int> (generator.generate());
    int y = static_cast<int> (generator.generate());
    x=abs(x);
    y=abs(y);
    switch (GhostType) {
    case 0: setTargetdestination(QPoint(x%216,y%240));break;
    case 1: setTargetdestination(QPoint(x%216+216,y%200));break;
    case 2: setTargetdestination(QPoint(x%216,y%240+240));break;
    case 3: setTargetdestination(QPoint(x%216+216,y%240+240));break;
    }
    }
    ChaseSettingTarget->start();


}

void ghost::setChaseModeTarget()
{   if (PhaseChanger<3 && !DeadMode && !WaitMode)
    {
    ScatterMode=false;
    ChaseMode = true;
    switch (GhostType) {
    case 0: ChaseModeDirectAttack();break;
    case 1: ChaseModeAttackFromBehind();break;
    case 2: ChaseModeAttackPredictedPosition();break;
    case 3: ChaseModeGetThenRunAwayTactics();break;
    }
    PhaseChanger++;
    }
    else if (isDead()) {
        setTargetdestination(QPoint(216,240));

    }
    else
    {
        ChaseSettingTarget->stop();
        ChaseMode=false;
        ScatterMode=true;
        PhaseChanger=0;
        setScatteredModeTarget();
    }
}

void ghost::setValnurableMode()
{
    if (!PowerballMode){
        PowerballMode=true;
    }
    else
    {
        PowerballMode=false;
    }

}

void ghost::GoOut()
{
    if (!pacmangame->isPowerBallMode())
    {
    if (WaitMode)
    {
        WaitMode=false;
        ScatterMode=true;
        DeadMode = false;
        ChaseMode = false;
        setScatteredModeTarget();


    }
    }
}

void ghost::ChaseModeDirectAttack()
{
    targetdestination=pacmangame->getPacmanPosition();

}

void ghost::ChaseModeAttackFromBehind()
{

    targetdestination=pacmangame->getPacmanLastMove();

}

void ghost::ChaseModeAttackPredictedPosition()
{

    int direction = pacmangame->getPacmanDirection();
    QPoint tempdirection=pacmangame->getPacmanPosition();
    switch (direction) {
    case 0: targetdestination=QPoint(tempdirection.x()+100,tempdirection.y());
                break;
    case 1: targetdestination=QPoint(tempdirection.x(),tempdirection.y()+100);
        break;
    case 2: targetdestination=QPoint(tempdirection.x()-100,tempdirection.y());
        break;
    case 3:targetdestination=QPoint(tempdirection.x(),tempdirection.y()-100);
        break;

    }
}

void ghost::ChaseModeGetThenRunAwayTactics()
{
    double DistanceToPacman=QLineF(pacmangame->getPacmanPosition(),pos()).length();
    if (DistanceToPacman>70){
        targetdestination=pacmangame->getPacmanPosition();
    }
    else {

        targetdestination=QPoint(432-pacmangame->getPacmanPosition().x(),504-pacmangame->getPacmanPosition().y());
    }

}

QPoint ghost::getNextMove() const
{
    return nextmove;
}

void ghost::UpdateOnCollisionPacman(pacman *_pac) const
{
if(!pacmangame->isPowerBallMode())
{
    _pac->setDead();
}
}

void ghost::UpdateOnCollisionGame(Game *_game) const
{
    if (!_game->isPowerBallMode())
    {

       _game->decreaseHealth();
    }

}

void ghost::UpdateOnCollisionObject()
{
 if(pacmangame->isPowerBallMode())
 {
     setDead();
 }
}

void ghost::animate()
{
    if (PowerballMode)
    {
        setPixmap(valnurable);
    }
    else if(VelocityX<0)
    {
        setPixmap(left);
    }
    else if(VelocityX>0)
    {
        setPixmap(right);
    }
    else if (VelocityY<0) {
        setPixmap(up);

    }
    else if (VelocityY>0) {
        setPixmap(down);
    }

}

QPoint ghost::getTargetdestination() const
{
return targetdestination;
}

void ghost::setTargetdestination(const QPoint &value)
{
    targetdestination = value;

}

void ghost::setVelocity(const QPoint & actualposition)
{
    VelocityX=0;
    VelocityY=0;
    if (!isDead())
    {
        Speed=2;

    }
    if(actualposition.x()-nextmove.x()<0)
    {
      VelocityX=Speed;
    }
    else if(actualposition.x()-nextmove.x()>0)
    {
      VelocityX=-Speed;
    }
    else if(actualposition.y()-nextmove.y()<0)
    {
        VelocityY=Speed;
    }
    else if(actualposition.y()-nextmove.y()>0)
    {

        VelocityY=-Speed;

    }

}

void ghost::ChooseNextMove()
{
   QPoint actualPosition = QPoint(static_cast<int> (pos().x()),static_cast<int>(pos().y()));
   QVector<QPoint> check = pacmangame->board->getMoveVector(actualPosition);
   for(int i =1; i<check.size();i++)
   {
       if (check[i]!=lastmove)
       {
           if(check[i]==QPoint(216,240)&& !DeadMode)
           {
               continue;
           }
           nextmove=check[i];
           break;
       }
   }
   QLineF linetotarget=QLine(nextmove,targetdestination);
   auto linelength=linetotarget.length();
   for(int i =1; i<check.size();i++)
   {
       if(check[i]!=lastmove)
       {
          if(QLineF(check[i],targetdestination).length() < linelength)
          {

              if(check[i]==QPoint(216,240)&& !DeadMode)
              {
                  continue;
              }
              linelength=QLineF(check[i],targetdestination).length();
              nextmove=check[i];
          }

       }
   }

}

bool ghost::isWaiting()
{
    return WaitMode;
}

bool ghost::isDead() const
{
    return DeadMode;
}

void ghost::setDead()
{
    DeadMode=true;
    targetdestination = QPoint (216,240);

}

void ghost::move()
{
      setPos(pos().x()+VelocityX,pos().y()+VelocityY);



  if (static_cast<int>(pos().x())==-24)
  {
   setPos(QPoint(456,240));
  }
  else if (static_cast<int>(pos().x())==456)
  {
      setPos(QPoint(-24,240));
  }
}

