#include "../inc/hyperball.h"
#include "../inc/game.h"
#include <QPen>
#include <QBrush>
Hyperball::Hyperball(QPoint pos)
{
    setRect(pos.x()+8,pos.y()+8,10,10);
    QPen PEN;
    QBrush BRUSH;
    PEN.setColor(Qt::red);
    PEN.setWidth(1);
    BRUSH.setColor(Qt::black);
    BRUSH.setStyle(Qt::SolidPattern);
    setPen(PEN);
    setBrush(BRUSH);
}

void Hyperball::UpdateOnCollisionGame(Game *_game) const
{
    _game->PowerBallActivation();
}

void Hyperball::UpdateOnCollisionObject()
{
    delete this;
}
