#include "../inc/ball.h"
#include <QPainter>
#include "../inc/game.h"

PointBall::PointBall(QPoint a)
{
position=a;
setRect(position.x()+12,position.y()+12,2,2);
QPen PEN;
PEN.setColor(Qt::blue);
PEN.setWidth(2);
setPen(PEN);
}

void PointBall::UpdateOnCollisionGame(Game *_game) const
{
    _game->increaseScore();
}

void PointBall::UpdateOnCollisionObject()
{
    delete this;
}

