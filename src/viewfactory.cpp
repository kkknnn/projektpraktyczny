#ifndef VIEWFACTORY_CPP
#define VIEWFACTORY_CPP
#include "../inc/viewfactory.h"

QVector<std::shared_ptr<QGraphicsView>> ViewFactory::ActualView{};



std::shared_ptr<ViewFactory> ViewFactory::getViewFactory(GamePhase view)
{
   switch (view)
   {
   case Menu: return std::make_shared<MainMenuFactory>();
   case PacmanGame: return std::make_shared<PacmanGameFactory>();
   }
   return nullptr;
}





#endif // VIEWFACTORY_CPP
