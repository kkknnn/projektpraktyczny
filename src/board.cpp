#include "../inc/board.h"
#include <QVector>
#include <QPoint>
#include <QDebug>
#include <algorithm>
PacmanBoard::PacmanBoard()
{
    crossroads.resize(68);
    crossroads[0].resize(3);
    crossroads[0]=QVector<QPoint>{QPoint(24,24),QPoint(96,24), QPoint(24,96)};
    crossroads[1].resize(4);
    crossroads[1]=QVector<QPoint>{QPoint(96,24),QPoint(24,24), QPoint(192,24),QPoint(96,96)};
    crossroads[2].resize(3);
    crossroads[2]=QVector<QPoint>{QPoint(192,24),QPoint(192,96), QPoint(96,24)};
    crossroads[3].resize(3);
    crossroads[3]=QVector<QPoint>{QPoint(240,24),QPoint(240,96), QPoint(336,24)};
    crossroads[4].resize(4);
    crossroads[4]=QVector<QPoint>{QPoint(336,24),QPoint(240,24),QPoint(408,24),QPoint(336,96)};// 240;24	408;24	336;96
    crossroads[5].resize(3);
    crossroads[5]=QVector<QPoint>{QPoint(408,24),QPoint(336,24), QPoint(408,96)};
    crossroads[6].resize(4);
    crossroads[6]=QVector<QPoint>{QPoint(24,96),QPoint(24,24), QPoint(24,144), QPoint(96,24)};
    crossroads[7].resize(5);
    crossroads[7]=QVector<QPoint>{QPoint(96,96),QPoint(96,24), QPoint(24,96),QPoint(144,96),QPoint(96,144)};
    crossroads[8].resize(4);
    crossroads[8]=QVector<QPoint>{QPoint(144,96),QPoint(192,96), QPoint(96,96),QPoint(144,144)};
    crossroads[9].resize(4); //192;96	192;24	144;96	240;96
    crossroads[9]=QVector<QPoint>{QPoint(192,96),QPoint(192,24), QPoint(144,96),QPoint(240,96)};
    crossroads[10].resize(4);//240;96	192;96	288;96	240;24
    crossroads[10]=QVector<QPoint>{QPoint(240,96),QPoint(192,96), QPoint(288,96),QPoint(240,24)};
    crossroads[11].resize(4);
    crossroads[12].resize(5);//    5
    crossroads[13].resize(4);//    4
    crossroads[14].resize(3);//    3
    crossroads[15].resize(4);//    4
    crossroads[16].resize(3);//    3
    crossroads[17].resize(3);//    3
    crossroads[18].resize(3);//    3
    crossroads[19].resize(3);//    3
    crossroads[20].resize(4);//    4
    crossroads[21].resize(3);//    3
    crossroads[22].resize(3);//    3
    crossroads[23].resize(4);//    4
    crossroads[24].resize(4);//    4
    crossroads[25].resize(4);//    4
    crossroads[26].resize(3);//    3
    crossroads[27].resize(2);//    2
    crossroads[28].resize(5);//    5
    crossroads[29].resize(4);//    4
    crossroads[30].resize(4);//    2
    crossroads[31].resize(4);//    4
    crossroads[32].resize(5);//    5
    crossroads[33].resize(2);//    2
    crossroads[34].resize(4);//    4
    crossroads[35].resize(4);//    4
    crossroads[36].resize(3);//    3
    crossroads[37].resize(4);//    4
    crossroads[38].resize(4);//    4
    crossroads[39].resize(3);//    3
    crossroads[40].resize(4);//    4
    crossroads[41].resize(3);//    3
    crossroads[42].resize(4);//    4
    crossroads[43].resize(4);//    4
    crossroads[44].resize(3);//    3
    crossroads[45].resize(3);//    3
    crossroads[46].resize(3);//    3
    crossroads[47].resize(3);//    3
    crossroads[48].resize(4);//    4
    crossroads[49].resize(3);//    3
    crossroads[50].resize(3);//    3
    crossroads[51].resize(4);//    4
    crossroads[52].resize(3);//    3
    crossroads[53].resize(4);//    4
    crossroads[54].resize(3);//    3
    crossroads[55].resize(3);//    3
    crossroads[56].resize(4);//    4
    crossroads[57].resize(4);//    4
    crossroads[58].resize(4);//    4
    crossroads[59].resize(3);//    3
    crossroads[60].resize(3);//    3
    crossroads[61].resize(4);//    4
    crossroads[62].resize(3);//    3
    crossroads[63].resize(3);//    3
    crossroads[64].resize(4);//    4
    crossroads[65].resize(3);//    3
    crossroads[66].resize(3);//    3
    crossroads[67].resize(4);//    4

crossroads[11]=QVector<QPoint>{QPoint(288,96),QPoint(240,96), QPoint(336,96),QPoint(288,144)};//    288;96	240;96	336;96	288;144
crossroads[12]=QVector<QPoint>{QPoint(336,96),QPoint(336,24), QPoint(408,96),QPoint(336,144),QPoint(288,96)};//    336;96	336;24	408;96	336;144	288;96
crossroads[13]=QVector<QPoint>{QPoint(408,96),QPoint(408,24), QPoint(408,144),QPoint(336,96)};//    408;96	408;24	408;144	336;96
crossroads[14]=QVector<QPoint>{QPoint(24,144),QPoint(24,96), QPoint(96,144)};//    24;144	24;96	96;144
crossroads[15]=QVector<QPoint>{QPoint(96,144),QPoint(96,96), QPoint(24,144),QPoint(96,240)};//    96;144	96;96	24;144	96;240
crossroads[16]=QVector<QPoint>{QPoint(144,144),QPoint(144,96), QPoint(192,144)};//    144;144	144;96	192;144
crossroads[17]=QVector<QPoint>{QPoint(192,144),QPoint(144,144), QPoint(192,192)};//    192;144	144;144	192;192
crossroads[18]=QVector<QPoint>{QPoint(240,144),QPoint(288,144), QPoint(240,192)};//    240;144	288;144	240;192
crossroads[19]=QVector<QPoint>{QPoint(288,144),QPoint(240,144), QPoint(288,96)};//    288;144	240;144	288;96
crossroads[20]=QVector<QPoint>{QPoint(336,144),QPoint(336,96), QPoint(408,144),QPoint(336,240)};//    336;144	336;96	408;144	336;240
crossroads[21]=QVector<QPoint>{QPoint(408,144),QPoint(408,96), QPoint(336,144)};//    408;144	408;96	336;144
crossroads[22]=QVector<QPoint>{QPoint(144,192),QPoint(192,192), QPoint(144,240)};//    144;192	192;192	144;240
crossroads[23]=QVector<QPoint>{QPoint(192,192),QPoint(144,192), QPoint(216,192),QPoint(192,144)};//    192;192	144;192	216;192	192;144
crossroads[24]=QVector<QPoint>{QPoint(216,192),QPoint(192,192), QPoint(240,192),QPoint(216,240)};//    216;192	192;192	240;192 216;240
crossroads[25]=QVector<QPoint>{QPoint(240,192),QPoint(240,144), QPoint(288,192),QPoint(216,192)};//    240;192	240;144	288;192	216;192
crossroads[26]=QVector<QPoint>{QPoint(288,192),QPoint(240,192), QPoint(288,240)};//    288;192	240;192	288;240
crossroads[27]=QVector<QPoint>{QPoint(-24,240),QPoint(96,240)};//    -24;240	96;240
crossroads[28]=QVector<QPoint>{QPoint(96,240),QPoint(-24,240), QPoint(144,240),QPoint(96,144),QPoint(96,336)};//    96;240	-24;240	144;240	96;144	96;336
crossroads[29]=QVector<QPoint>{QPoint(144,240),QPoint(144,192), QPoint(96,240),QPoint(144,312)};//    144;240	144;192	96;240	144;312
crossroads[30]=QVector<QPoint>{QPoint(216,240),QPoint(216,192)};//    216;240	216;192
crossroads[31]=QVector<QPoint>{QPoint(288,240),QPoint(288,192), QPoint(336,240),QPoint(288,312)};//    288;240	288;192	336;240	288;312
crossroads[32]=QVector<QPoint>{QPoint(336,240),QPoint(336,144), QPoint(456,240),QPoint(336,336),QPoint(288,240)};//    336;240	336;144	456;240	336;336	288;240
crossroads[33]=QVector<QPoint>{QPoint(456,240),QPoint(336,240)};//    456;240	336;240
crossroads[34]=QVector<QPoint>{QPoint(144,312),QPoint(144,240), QPoint(288,312),QPoint(144,360)};//    144;312	144;240	288;312	144;360
crossroads[35]=QVector<QPoint>{QPoint(288,312),QPoint(144,312), QPoint(288,360),QPoint(288,240)};//    288;312	144;312	288;360	288;240
crossroads[36]=QVector<QPoint>{QPoint(24,336),QPoint(24,384), QPoint(96,336)};//    24;336	24;384	96;336
crossroads[37]=QVector<QPoint>{QPoint(96,336),QPoint(24,336), QPoint(96,360),QPoint(96,240)};//    96;336	24;336	96;360, 96;240
crossroads[38]=QVector<QPoint>{QPoint(336,336),QPoint(336,240), QPoint(408,336),QPoint(336,360)};//    336;336	336;240	408;336	336;360
crossroads[39]=QVector<QPoint>{QPoint(408,336),QPoint(408,384), QPoint(336,336)};//    408;336	408;384	336;336
crossroads[40]=QVector<QPoint>{QPoint(144,360),QPoint(144,312), QPoint(192,360),QPoint(96,360)};//    144;360	144;312	192;360	96;360
crossroads[41]=QVector<QPoint>{QPoint(240,360),QPoint(240,432), QPoint(288,360)};//    240;360	240;432	288;360
crossroads[42]=QVector<QPoint>{QPoint(288,360),QPoint(240,360), QPoint(288,312),QPoint(336,360)};//    288;360	240;360	288;312	336;360
crossroads[43]=QVector<QPoint>{QPoint(336,360),QPoint(288,360), QPoint(336,336),QPoint(336,408)};//    336;360	288;360	336;336	336;408
crossroads[44]=QVector<QPoint>{QPoint(24,384),QPoint(24,336), QPoint(48,384)};//    24;384	24;336	48;384
crossroads[45]=QVector<QPoint>{QPoint(48,384),QPoint(48,432), QPoint(24,384)};//    48;384	48;432	24;384
crossroads[46]=QVector<QPoint>{QPoint(384,384),QPoint(408,384), QPoint(384,432)};//    384;384	408;384	384;432
crossroads[47]=QVector<QPoint>{QPoint(408,384),QPoint(408,336), QPoint(384,384)};//    408;384	408;336	384;384
crossroads[48]=QVector<QPoint>{QPoint(96,408),QPoint(96,432), QPoint(144,408),QPoint(96,360)};//    96;408	96;432	144;408	96;360
crossroads[49]=QVector<QPoint>{QPoint(144,408),QPoint(144,432), QPoint(96,408)};//    144;408	144;432	96;408
crossroads[50]=QVector<QPoint>{QPoint(288,408),QPoint(288,432), QPoint(336,408)};//    288;408	288;432	336;408
crossroads[51]=QVector<QPoint>{QPoint(336,408),QPoint(336,432), QPoint(288,408),QPoint(336,360)};//    336;408	336;432	288;408 336;360
crossroads[52]=QVector<QPoint>{QPoint(24,432),QPoint(24,480), QPoint(48,432)};//    24;432	24;480	48;432
crossroads[53]=QVector<QPoint>{QPoint(48,432),QPoint(48,384), QPoint(24,432),QPoint(96,432)};//    48;432	48;384	24;432	96;432
crossroads[54]=QVector<QPoint>{QPoint(96,432),QPoint(48,432), QPoint(96,408)};//    96;432	48;432	96;408
crossroads[55]=QVector<QPoint>{QPoint(144,432),QPoint(192,432), QPoint(144,408)};//    144;432	192;432	144;408
crossroads[56]=QVector<QPoint>{QPoint(192,432),QPoint(216,432), QPoint(192,360),QPoint(144,432)};//    192;432	216;432	192;360	144;432
crossroads[57]=QVector<QPoint>{QPoint(216,432),QPoint(216,480), QPoint(192,432),QPoint(240,432)};//    216;432	216;480	192;432	240;432
crossroads[58]=QVector<QPoint>{QPoint(240,432),QPoint(240,360), QPoint(216,432),QPoint(288,432)};//    240;432	240;360	216;432	288;432
crossroads[59]=QVector<QPoint>{QPoint(288,432),QPoint(288,408), QPoint(240,432)};//    288;432	288;408	240;432
crossroads[60]=QVector<QPoint>{QPoint(336,432),QPoint(336,408), QPoint(384,432)};//    336;432	336;408	384;432
crossroads[61]=QVector<QPoint>{QPoint(384,432),QPoint(384,384), QPoint(408,432),QPoint(336,432)};//    384;432	384;384	408;432	336;432
crossroads[62]=QVector<QPoint>{QPoint(408,432),QPoint(408,480), QPoint(384,432)};//    408;432	408;480	384;432
crossroads[63]=QVector<QPoint>{QPoint(24,480),QPoint(24,432), QPoint(216,480)};//    24;480	24;432	216;480
crossroads[64]=QVector<QPoint>{QPoint(216,480),QPoint(24,480), QPoint(216,432),QPoint(408,480)};//    216;480	48;432	216;432	408;480
crossroads[65]=QVector<QPoint>{QPoint(408,480),QPoint(408,432), QPoint(216,480)};//    408;480	408;432	216;480
crossroads[66]=QVector<QPoint>{QPoint(192,360),QPoint(144,360), QPoint(192,432)};//    192;360	144;360	192;432
crossroads[67]=QVector<QPoint>{QPoint(96,360),QPoint(96,336),QPoint(144,360),QPoint(96,408)};//    96;360	96;336	144;360	96;408


for (int i =0;i<crossroads.size();i++) {

    int tempx = crossroads[i][0].x();
    int tempy = crossroads[i][0].y();
    if (tempx==216 && tempy == 240)
    {
        continue;
    }
    QPoint temppoint = QPoint(tempx,tempy);
    if (!PointBallLocations.contains(temppoint))
    {

    PointBallLocations.push_back(temppoint);

     }

    for (int j=1;j<crossroads[i].size();j++)
    {
      int tempdiffx = 0;
      int tempdiffy = 0;
       tempdiffx = tempx-crossroads[i][j].x();
       tempdiffy = tempy-crossroads[i][j].y();
      if (tempdiffx>0)
      {
          for(int k=std::abs(tempdiffx/24);k>0;k--)
          {

           auto temp =QPoint(crossroads[i][j].x()+(tempdiffx-k*24),crossroads[i][j].y());
           if (!PointBallLocations.contains(temp))
           {

           PointBallLocations.push_back(temp);

            }
          }
      }
      else if (tempdiffy>0)
      {

          for(int k=std::abs(tempdiffy/24);k>0;k--)
          {
              auto temp = QPoint(crossroads[i][j].x(),crossroads[i][j].y()+(tempdiffy-k*24));
           if (!PointBallLocations.contains(temp))
           {

           PointBallLocations.push_back(temp);
           }
          }

      }
    }


}
}

QVector<QPoint> PacmanBoard::getMoveVector(QPoint & searchpoint)
{
 for (int i =0; i<crossroads.size() ;i++)
 {
     if(crossroads[i][0]==searchpoint)
     {
         return crossroads[i];
     }
 }
 QVector<QPoint> a;
 return a;
}

void PacmanBoard::addMoveVector(QVector<QPoint> & newmove)
{

    int oldsize = crossroads.size();
   crossroads.resize(oldsize+1);
   crossroads[oldsize].resize(newmove.size());
   crossroads[oldsize]=newmove;


}

QVector<QPoint> PacmanBoard::getAllCrossroads()
{
    QVector<QPoint> VectorToReturn;
    for (int i=0;i<crossroads.size();i++)
    {
        VectorToReturn<<crossroads[i][0];
    }
    return VectorToReturn;

}

bool PacmanBoard::checkIfCrossroad(QPoint &checkPoint)
{
    for (int i =0; i<crossroads.size() ;i++)
    {
        if(crossroads[i][0].x()==checkPoint.x() && crossroads[i][0].y()==checkPoint.y())
        {

            return true;
        }
    }
    return false;



}

QVector<QPoint> PacmanBoard::getPointBallLocations() const
{
    return PointBallLocations;
}

QPoint PacmanBoard::getPointballSingleLocation(int number) const
{
    auto toreturn=PointBallLocations[number];
    return toreturn;
}

