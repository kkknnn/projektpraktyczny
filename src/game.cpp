#include "../inc/game.h"
#include <iostream>
#include <QGraphicsScene>
#include <QRect>
#include <QTimer>
#include <QSet>
#include <QHash>
#include <QMediaPlaylist>
#include <sstream>
#include <QDebug>
#include <QMessageBox>
#include "../inc/globalvariables.h"
#include "../inc/hyperball.h"
#include "../inc/pacman.h"
#include "../inc/ball.h"
#include "../inc/viewfactory.h"
#include "../inc/sysfunctions.h"

//inline uint qHash (const QPoint & key)
//{
//    return qHash (QPair<int,int>(key.x(), key.y()) );
//}

Game::Game(MainMenuInterface *mmenu, SysFuctionInterface * syst, QWidget * parent): QGraphicsView (parent)
{

    player = new pacman(this);
    player->setFlag(QGraphicsItem::ItemIsFocusable);
    player->setFocus();
    board=new PacmanBoard;
    powerballmode=false;
    System.reset(syst);
    QGraphicsPixmapItem * backgroundboard = new QGraphicsPixmapItem();
    backgroundboard->setPixmap(QPixmap(":/img/img/board.png"));
    backgroundboard->setPos(0,0);
    QGraphicsPixmapItem * backgroundright = new QGraphicsPixmapItem();
    backgroundright->setPixmap(QPixmap(":/img/img/bacgroundright.png"));
    backgroundright->setPos(456,0);
    QGraphicsPixmapItem * backgroundown = new QGraphicsPixmapItem();
    backgroundown->setPixmap(QPixmap(":/img/img/bacgrounddown.png"));
    backgroundown->setPos(0,528);
    GoOutSequencer=0;
    Score = -1;
    Health = 3;
    Menu.reset(mmenu);
    PacmanGame = new QGraphicsScene(this);
    GhostArray.resize(4);
    PacmanGame->addItem(backgroundboard);
    ScoreLabel = new QLabel();
    LifeLabel = new QLabel();
    std::stringstream labelstream;
    labelstream << "Score: " << Score;
    std::string textlabel = labelstream.str();

    ScoreLabel->setText(QString::fromStdString(textlabel));

    ScoreLabel->setGeometry(500,150,100,20);
    ScoreLabel->setFont(QFont("arial"));


    textlabel = "Health " + std::to_string(Health);

    LifeLabel->setText(QString::fromStdString(textlabel));

    LifeLabel->setGeometry(500,200,100,20);
    LifeLabel->setFont(QFont("arial"));



    for (int i = 0; i<board->getPointBallLocations().size();i++)
    {
        PacmanGame->addItem(new PointBall(board->getPointballSingleLocation(i)));
    }
    for(int i=0;i<GhostArray.size();i++)
    {
        GhostArray[i]=new ghost(this,i);

    }
    for (int i=3;i>=0;i--)
    {
        PacmanGame->addItem(GhostArray[i]);
    }
    PacmanGame->addItem(player);

    PacmanGame->addItem(backgroundown);
    PacmanGame->addItem(backgroundright);
    PacmanGame->addWidget(LifeLabel);
    PacmanGame->addWidget(ScoreLabel);

    setScene(PacmanGame);
    PacmanGame->setSceneRect(0,0,800,600);
    System->showView(this);
    GhostMoveTimer = new QTimer(this);
    PowerballMode = new QTimer(this);
    GhostMoveOut = new QTimer (this);
    connect(GhostMoveTimer,SIGNAL(timeout()),this,SLOT(UpdateAllObjects()));
    connect(GhostMoveOut,SIGNAL(timeout()),this,SLOT(GhostRelease()));
    GhostMoveTimer->start(50);
    GhostMoveOut->start(5000);
    PowerballMode->setInterval(10000);
    connect(PowerballMode,SIGNAL(timeout()),this,SLOT(PowerBallActivation()));

    Hyperball * hyp1 = new Hyperball(QPoint(24,24));
    Hyperball * hyp2 = new Hyperball(QPoint(24,480));
    Hyperball * hyp3 = new Hyperball(QPoint(408,24));
    Hyperball * hyp4 = new Hyperball(QPoint(408,480));

    PacmanGame->addItem(hyp1);
    PacmanGame->addItem(hyp2);
    PacmanGame->addItem(hyp3);
    PacmanGame->addItem(hyp4);
   setFixedSize(static_cast<int>(PacmanGame->width()),static_cast<int>(PacmanGame->height()));
   setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
   setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);

   BackgroundMusic=std::make_shared<QMediaPlayer> (new QMediaPlayer());
   BackgroundSound=std::make_shared<QMediaPlaylist> (new QMediaPlaylist());
   BackgroundSound->addMedia(QUrl("qrc:/audio/audio/backgroundnormalmode.mp3"));
   BackgroundSound->setPlaybackMode(QMediaPlaylist::PlaybackMode::Loop);
   BackgroundMusic->setPlaylist(BackgroundSound.get());
   BackgroundMusic->setVolume(20);
   BackgroundMusic->play();
   PowerballModeMusic=std::make_shared<QMediaPlayer> (new QMediaPlayer());

}

Game::Game(QVector<GhostInterface *> ghostvector, PacmanInterface * pacmanplayer, BoardInterface * GameBoard, SysFuctionInterface * sys, MainMenuInterface *men, QWidget *parent): QGraphicsView (parent)
{
    player = pacmanplayer;
    player->setFlag(QGraphicsItem::ItemIsFocusable);
    player->setFocus();
    board=GameBoard;
    System.reset(sys);
    Menu.reset(men);
    powerballmode=false;
    QGraphicsPixmapItem * backgroundboard = new QGraphicsPixmapItem();
    backgroundboard->setPixmap(QPixmap(":/img/img/board.png"));
    backgroundboard->setPos(0,0);
    QGraphicsPixmapItem * backgroundright = new QGraphicsPixmapItem();
    backgroundright->setPixmap(QPixmap(":/img/img/bacgroundright.png"));
    backgroundright->setPos(456,0);
    QGraphicsPixmapItem * backgroundown = new QGraphicsPixmapItem();
    backgroundown->setPixmap(QPixmap(":/img/img/bacgrounddown.png"));
    backgroundown->setPos(0,528);
    GoOutSequencer=0;
    Score = -1;
    Health = 3;
    PacmanGame = new QGraphicsScene(this);
    GhostArray.resize(4);
    PacmanGame->addItem(backgroundboard);
    ScoreLabel = new QLabel();
    LifeLabel = new QLabel();
    std::stringstream labelstream;
    labelstream << "Score: " << Score;
    std::string textlabel = labelstream.str();
    ScoreLabel->setText(QString::fromStdString(textlabel));
    ScoreLabel->setGeometry(500,150,100,20);
    ScoreLabel->setFont(QFont("arial"));


    textlabel = "Health " + std::to_string(Health);
    LifeLabel->setText(QString::fromStdString(textlabel));
    LifeLabel->setGeometry(500,200,100,20);
    LifeLabel->setFont(QFont("arial"));



    for (int i = 0; i<board->getPointBallLocations().size();i++)
    {
        PacmanGame->addItem(new PointBall(board->getPointballSingleLocation(i)));
    }
    GhostArray=ghostvector;

    for (int i=3;i>=0;i--)
    {
        PacmanGame->addItem(GhostArray[i]);
    }
    PacmanGame->addItem(player);

    PacmanGame->addItem(backgroundown);
    PacmanGame->addItem(backgroundright);
    PacmanGame->addWidget(LifeLabel);
    PacmanGame->addWidget(ScoreLabel);

    setScene(PacmanGame);
    PacmanGame->setSceneRect(0,0,800,600);
    System->showView(this);
    GhostMoveTimer = new QTimer(this);
    PowerballMode = new QTimer(this);
    GhostMoveOut = new QTimer (this);
    connect(GhostMoveTimer,SIGNAL(timeout()),this,SLOT(UpdateAllObjects()));
    connect(GhostMoveOut,SIGNAL(timeout()),this,SLOT(GhostRelease()));
    GhostMoveTimer->start(50);
    GhostMoveOut->start(5000);
    PowerballMode->setInterval(10000);
    connect(PowerballMode,SIGNAL(timeout()),this,SLOT(PowerBallActivation()));

    Hyperball * hyp1 = new Hyperball(QPoint(24,24));
    Hyperball * hyp2 = new Hyperball(QPoint(24,480));
    Hyperball * hyp3 = new Hyperball(QPoint(408,24));
    Hyperball * hyp4 = new Hyperball(QPoint(408,480));

    PacmanGame->addItem(hyp1);
    PacmanGame->addItem(hyp2);
    PacmanGame->addItem(hyp3);
    PacmanGame->addItem(hyp4);
    setFixedSize(static_cast<int>(PacmanGame->width()),static_cast<int>(PacmanGame->height()));
    setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);

    BackgroundMusic=std::make_shared<QMediaPlayer> (new QMediaPlayer());
    BackgroundSound=std::make_shared<QMediaPlaylist> (new QMediaPlaylist());
    BackgroundSound->addMedia(QUrl("qrc:/audio/audio/backgroundnormalmode.mp3"));
    BackgroundSound->setPlaybackMode(QMediaPlaylist::PlaybackMode::Loop);
    BackgroundMusic->setPlaylist(BackgroundSound.get());
    BackgroundMusic->setVolume(20);
    BackgroundMusic->play();
    PowerballModeMusic=std::make_shared<QMediaPlayer> (new QMediaPlayer());

}

bool Game::addObject(QGraphicsItem * itemtoaddtoscene)
{

    if (itemtoaddtoscene!=nullptr)
    {
        PacmanGame->addItem(itemtoaddtoscene);
        return true;
    }
    else {
        return false;
    }
}


QPoint Game::getPacmanPosition()
{
   return QPoint(static_cast<int>(player->pos().x()),static_cast<int> (player->pos().y()));

}

QPoint Game::getPacmanLastMove()
{
    return player->getLastMove();
}

QVector<GhostInterface *> Game::getAllGhosts() const
{
    return GhostArray;
}

int Game::getPacmanDirection()
{
    return player->getActualDirection();
}

bool Game::isPowerBallMode()
{
    return powerballmode;
}

Game::~Game()
{

    auto VectorOfExistingObjects=scene()->items();
    for (int i=0;i<VectorOfExistingObjects.size();i++)
    {


        if(VectorOfExistingObjects[i]!=nullptr)
        {

            delete VectorOfExistingObjects[i];
        }
    }


    PacmanGame->clear();
\

    delete PacmanGame;
    delete board;



}

void Game::GhostRelease()
{
    if (GoOutSequencer==GhostArray.size())
    {
        GoOutSequencer=0;
    }

    for (int i=GoOutSequencer;i<GhostArray.size();i++)
    {
        if(GhostArray[i]->isWaiting() && !powerballmode)
        {
            GhostArray[i]->GoOut();
            GoOutSequencer++;
            return;
        }

    }

}

void Game::UpdateAllObjects()
{

    player->update();

    for (int i=0;i<GhostArray.size();i++)
    {
        GhostArray[i]->update();
    }



}

void Game::PowerBallActivation()
{

    for (int i=0;i<GhostArray.size();i++)
    {
        GhostArray[i]->setValnurableMode();
    }
    if (PowerballMode->isActive())
    {
        PowerballMode->stop();
        GhostMoveOut->start();
        powerballmode=false;
    }
    else {
         PowerballMode->start();
         GhostMoveOut->stop();
         powerballmode=true;
    }


}

void Game::ActualiseTopPlayers()
{
    System->initFinalMessage(this);

    auto TopPlayersVec=Menu->getTopPlayers();
    QString PlayersName = Menu->getPlayerNickname();
    int TopPlayersNum=Menu->getTopPlayers().size();

    if(TopPlayersNum<5)
    {
        TopPlayersVec.push_back(qMakePair<QString,int>(PlayersName,Score));
        System->setFinalMessageWin();
        System->showFinalMessage();
    }
    else if (getScore()==189)
    {
        TopPlayersVec[TopPlayersNum-1].first=PlayersName;
        TopPlayersVec[TopPlayersNum-1].second=Score;
        System->setFinalMessageWin();
        System->showFinalMessage();

    }
    else if (TopPlayersVec[TopPlayersNum-1].second<Score)
    {

        TopPlayersVec[TopPlayersNum-1].first=PlayersName;
        TopPlayersVec[TopPlayersNum-1].second=Score;
        System->setFinalMessageWin();
        System->showFinalMessage();

    }
    else
    {
        System->setFinalMessageLoose();
        System->showFinalMessage();

        return;
    }

    System->SaveBestPlayers(TopPlayersVec,QString("/home/iks/ProjektPraktyczny/data/bestplayers.dat"));

}

void Game::GameOver()
{

    GhostMoveTimer->stop();
    GhostMoveOut->stop();
    PowerballMode->stop();
    BackgroundMusic->stop();

    ActualiseTopPlayers();
    this->close();
    System->MenuInit();
}

int Game::getHealth() const
{
    return Health;
}


void Game::increaseScore()
{

    Score++;
    std::string textlabel = "Score " + std::to_string(Score);
    ScoreLabel->setText(QString::fromStdString(textlabel));
    if (getScore()==189)
    {
        GameOver();
        return;
    }
}

int Game::getScore() const
{
    return Score;
}

void Game::decreaseHealth()
{
    Health--;
    std::string textlabel = "Health " + std::to_string(Health);
    LifeLabel->setText(QString::fromStdString(textlabel));
    if (getHealth()==0)
    {
        GameOver();
        return;
    }

}


