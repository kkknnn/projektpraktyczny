#include <QKeyEvent>
#include <QDebug>
#include <QVector>
#include <QList>
#include <QPoint>
#include <QThread>
#include "../inc/pacman.h"
#include "../inc/game.h"
#include "../inc/ghost.h"
#include "../inc/ball.h"
#include "../inc/hyperball.h"

pacman::pacman(Game * _game)
{
    EatHyperBallSound=std::unique_ptr<QMediaPlayer> (new QMediaPlayer());
    EatBallSound=std::unique_ptr<QMediaPlayer> (new QMediaPlayer());
    PacmanDeadSound=std::unique_ptr<QMediaPlayer>(new QMediaPlayer());
    EatHyperBallSound->setMedia(QUrl("qrc:/audio/audio/paceatpowerball.wav"));
    EatBallSound->setMedia(QUrl("qrc:/audio/audio/paceatball.mp3"));
    PacmanDeadSound->setMedia(QUrl("qrc:/audio/audio/pacdead.wav"));
    AnimationPhase=0;
    ActualDirection=0;
    AnimationHelper=1;
    lastmove=QPoint(-24,240);
    setPos(0,240);
    pacmangame=_game;
    PosibleMoveUp=false;
    PosibleMoveDown=false;
    PosibleMoveLeft=true;
    PosibleMoveRight=true;
    Speed=8;
    up1=QPixmap(":/img/img/pacman_pics/pacman pics/pacup1.png");
    up2=QPixmap(":/img/img/pacman_pics/pacman pics/pacup2.png");
    up3=QPixmap(":/img/img/pacman_pics/pacman pics/pacup3.png");
    down1=QPixmap(":/img/img/pacman_pics/pacman pics/pacdown1.png");
    down2=QPixmap(":/img/img/pacman_pics/pacman pics/pacdown2.png");
    down3=QPixmap(":/img/img/pacman_pics/pacman pics/pacdown3.png");
    left1=QPixmap(":/img/img/pacman_pics/pacman pics/pacleft1.png");
    left2=QPixmap(":/img/img/pacman_pics/pacman pics/pacleft2.png");
    left3=QPixmap(":/img/img/pacman_pics/pacman pics/pacleft3.png");
    right1=QPixmap(":/img/img/pacman_pics/pacman pics/pacright1.png");
    right2=QPixmap(":/img/img/pacman_pics/pacman pics/pacright2.png");
    right3=QPixmap(":/img/img/pacman_pics/pacman pics/pacright3.png");
    setPixmap(right1);

}



void pacman::update()
{

     QList<QGraphicsItem*> ColidingWithPacman = collidingItems();
     CollisionReact(ColidingWithPacman);
}

int pacman::getAnimationPhase() const
{
    return AnimationPhase;
}

void pacman::setAnimationPhase(int value)
{
    AnimationPhase = value;
}

void pacman::animate()
{
    switch (ActualDirection) {

    case 0: // patrzy w prawo
        if(AnimationPhase==0)
        {
        setPixmap(right1);
        AnimationHelper=1;
        }
        else if (AnimationPhase==1)
        {
        setPixmap(right2);
        }
        else
        {
        setPixmap(right3);
        AnimationHelper=-1;
        }
        AnimationPhase+=AnimationHelper;
        break;
    case 1:   // patrzy w dol
        if(AnimationPhase==0)
        {
        setPixmap(down1);
        AnimationHelper=1;
        }
        else if (AnimationPhase==1)
        {
        setPixmap(down2);
        }
        else
        {
        setPixmap(down3);
        AnimationHelper=-1;
        }
        AnimationPhase+=AnimationHelper;

    break;
    case 2:// patrzy w lewo
        if(AnimationPhase==0)
        {
        setPixmap(left1);
        AnimationHelper=1;
        }
        else if (AnimationPhase==1)
        {
        setPixmap(left2);
        }
        else
        {
        setPixmap(left3);
        AnimationHelper=-1;
        }
        AnimationPhase+=AnimationHelper;

        break;
    case 3:// patrzy w gore
        if(AnimationPhase==0)
        {
        setPixmap(up1);
        AnimationHelper=1;
        }
        else if (AnimationPhase==1)
        {
        setPixmap(up2);
        }
        else
        {
        setPixmap(up3);
        AnimationHelper=-1;
        }
        AnimationPhase+=AnimationHelper;
        break;


    }
}

int pacman::getActualDirection() const
{
    return ActualDirection;
}

QPoint pacman::getLastMove() const
{
    return lastmove;
}


void pacman::movePosibilityCheck(QPoint actualPosition)
{

    QVector<QPoint> check = pacmangame->board->getMoveVector(actualPosition);

    for (int i=1;i<check.size();i++)
    {
        if (actualPosition.x()-check[i].x()<0)
        {

            PosibleMoveRight=true;
        }
        if (actualPosition.x()-check[i].x()>0)
        {

            PosibleMoveLeft=true;
        }
        if (actualPosition.y()-check[i].y()>0)
        {

            PosibleMoveUp=true;
        }
        if (actualPosition.y()-check[i].y()<0)
        {

            PosibleMoveDown=true;
        }
    }
}




void pacman::keyPressEvent(QKeyEvent *event)
{


    if(event->key()==Qt::Key_Left && PosibleMoveLeft)
    {

        setPos(x()-Speed,y());
        PosibleMoveUp=false;
        PosibleMoveDown=false;
        PosibleMoveRight=true;
        ActualDirection=2;
        animate();

    }
    else if(event->key()==Qt::Key_Right && PosibleMoveRight)
    {

        setPos(x()+Speed,y());
        PosibleMoveUp=false;
        PosibleMoveDown=false;
        PosibleMoveLeft=true;
        ActualDirection=0;
        animate();

    }
    else if(event->key()==Qt::Key_Up && PosibleMoveUp)
    {

        setPos(x(),y()-Speed);

        PosibleMoveDown=true;
        PosibleMoveLeft=false;
        PosibleMoveRight=false;
        ActualDirection=3;
        animate();

    }

    else if(event->key()==Qt::Key_Down && PosibleMoveDown)
    {

        setPos(x(),y()+Speed);
        PosibleMoveUp=true;
        PosibleMoveLeft=false;
        PosibleMoveRight=false;
        ActualDirection=1;
        animate();


    }

    if (static_cast<int>(pos().x())==-24 && ActualDirection==2)
    {
     setPos(QPoint(456,240));
    }
    else if (static_cast<int>(pos().x())==456 && ActualDirection==0)
    {
      setPos(QPoint(-24,240));
    }

    QPoint actualposition = QPoint(int(this->pos().x()),int(pos().y()));

    if(pacmangame->board->checkIfCrossroad(actualposition))
    {
     PosibleMoveUp=false;
     PosibleMoveDown=false;
     PosibleMoveLeft=false;
     PosibleMoveRight=false;
     lastmove=actualposition;
     movePosibilityCheck(actualposition);

    }
}

QPoint pacman::getPosition()
{
    return QPoint(static_cast<int>(pos().x()),static_cast<int>(pos().y()));

}

void pacman::setPosition(QPoint newposition)
{
    setPos(newposition);
}

void pacman::CollisionReact(QList<QGraphicsItem *> & ColidingWithPacman)
{
    for (auto i=0;i<ColidingWithPacman.size();i++)
    {


       auto UpdatableWithPacman = dynamic_cast<CollisionUpdateAffectsPacman*>(ColidingWithPacman[i]);
       if (UpdatableWithPacman!=nullptr)
          {

                 UpdatableWithPacman->UpdateOnCollisionPacman(this);
          }


       auto UpdatableWithGame = dynamic_cast<CollisionUpdateAffectsGame*>(ColidingWithPacman[i]);
       if (UpdatableWithGame!=nullptr)
       {

           UpdatableWithGame->UpdateOnCollisionGame(pacmangame);
       }

       if (typeid (*ColidingWithPacman[i])==typeid (PointBall))
       {

           if (EatBallSound->state()==QMediaPlayer::State::PlayingState)
           {
               EatBallSound->setPosition(0);
           }
           EatBallSound->play();
       }

       if (typeid (*ColidingWithPacman[i])==typeid (Hyperball))
       {
           EatHyperBallSound->play();
       }

       auto UpdatableForObject = dynamic_cast<CollisionUpdateAffectsObject*>(ColidingWithPacman[i]);
           if (UpdatableForObject!=nullptr)
           {

                        UpdatableForObject->UpdateOnCollisionObject();
           }
    }

}


void pacman::setDead()
{

    if(pos().x()<96 && pos().y()==240.0)
    {
        lastmove=QPoint(456,240);
        setPos(360,240);


    }
    else
    {
        lastmove=QPoint(-24,240);
        setPos(0,240);

    }
    PacmanDeadSound->play();
    PosibleMoveUp=false;
    PosibleMoveDown=false;
    PosibleMoveLeft=true;
    PosibleMoveRight=true;


}

