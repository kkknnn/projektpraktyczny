#include "../inc/sysfunctions.h"
#include "../inc/viewfactory.h"
#include <QFile>
#include <QDebug>



void SysFuction::showView(QGraphicsView *viewtoshow)
{
 viewtoshow->show();
}




QList<QPair<QString, int> > SysFuction::ReadBestPlayers(const QString &fileadress)
{
    QFile database(fileadress);
    database.open(QIODevice::ReadOnly | QIODevice::Text);
    if(!database.isOpen())
    {
        qDebug() << "file to write only";

    }

    QTextStream filestream(&database);

    QString player;
    QString points;
    QList<QPair<QString,int>> TableOfBestPlayers;
    int maxread=1;
    while(maxread < 6 && !filestream.atEnd())
    {
    player=filestream.readLine(30);
    points=filestream.readLine(30);
    int intpoints = points.toInt();
    TableOfBestPlayers.push_back(qMakePair(player,intpoints));
    maxread++;
    player="";
    points="";
    }

    for (int i = 0;i<TableOfBestPlayers.size();i++)
    {
        for(int j = 1; j<TableOfBestPlayers.size()-i;j++)
            if(TableOfBestPlayers[j].second>TableOfBestPlayers[j-1].second)
            {
                std::swap(TableOfBestPlayers[j],TableOfBestPlayers[j-1]);
            }
    }
    return TableOfBestPlayers;
}

void SysFuction::initFinalMessage(QGraphicsView *parent)
{
    if (FinalMessage==nullptr)
    {
    FinalMessage = std::make_shared<QMessageBox>(parent);
    }
    else
    {
    FinalMessage.reset();
    FinalMessage = std::make_shared<QMessageBox>(parent);
    }

    FinalMessage->setWindowTitle("Game Over");
    FinalMessage->setGeometry(400,400,500,500);
    FinalMessage->setStandardButtons(QMessageBox::Ok);


}

void SysFuction::setFinalMessageWin()
{
    FinalMessage->setText(QString("Congrats! you have qualified to best players"));
    FinalMessage->setIconPixmap(QPixmap(":/img/img/PacWinIcon.png"));
}

void SysFuction::setFinalMessageLoose()
{

    FinalMessage->setText(QString("Congrats! you have qualified to best players list!"));
    FinalMessage->setIconPixmap(QPixmap(":/img/img/PacWinIcon.png"));
}

void SysFuction::SaveBestPlayers(QList<QPair<QString, int> > &BestPlayers, const QString &fileadress)
{

    QFile database(fileadress);
    database.open(QFile::WriteOnly|QFile::Truncate);
    if (!database.isOpen())
    {
        qDebug()<< "Problem with writing to file" << '\n';
        return;
    }
    QTextStream filestream(&database);

    for (int i = 0; i<BestPlayers.size();i++)
    {

        filestream << BestPlayers[i].first << '\n';
        filestream << BestPlayers[i].second << '\n';

    }

}

void SysFuction::showFinalMessage() const
{
    FinalMessage->exec();
}

void SysFuction::GameInit() const
{
    auto newView = ViewFactory::getViewFactory(GamePhase::PacmanGame);
    newView->createView();
}

void SysFuction::MenuInit() const
{
    auto newView = ViewFactory::getViewFactory(GamePhase::Menu);
    newView->createView();

}
