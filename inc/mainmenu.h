#ifndef MAINMENU_H
#define MAINMENU_H
#include <QGraphicsView>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include "sysfunctions.h"
#include <memory>



class MainMenuInterface
{
public:
    virtual ~MainMenuInterface()=default;
  //  virtual void LoadBestPlayerList() = 0;
    virtual QList<QPair<QString, int> > getTopPlayers() const = 0;
    virtual QString getPlayerNickname() const = 0;
    virtual void GameInit()=0;
};


class MainMenu: public QGraphicsView, public MainMenuInterface
{

Q_OBJECT

public:
MainMenu(QWidget * parent = nullptr);
MainMenu(SysFuctionInterface * Sys,QWidget * parent = nullptr );
~MainMenu() override = default;
//void LoadBestPlayerList() override;
QList<QPair<QString, int> > getTopPlayers() const override;
QString getPlayerNickname() const override;

public slots:
void GameInit() override;


private:
std::shared_ptr<QGraphicsScene> MainMenuScene;
std::shared_ptr<QPushButton> NewGameButton;
std::shared_ptr<QPushButton> QuitGameButton;
std::shared_ptr<QLineEdit> PlayerName;
std::shared_ptr<QLabel> BestPlayerLabel;
std::shared_ptr<QGraphicsPixmapItem> PacmanImage;
std::shared_ptr<QGraphicsPixmapItem> GhostImage;
std::shared_ptr<SysFuctionInterface> System;
QList<QPair<QString,int>> TopPlayers;
QString PlayerNickname{};
};


#endif // MAINMENU_H
