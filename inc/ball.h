#ifndef BALL_H
#define BALL_H
#include <QGraphicsEllipseItem>
#include <QPaintEvent>
#include "updatableoncollision.h"


class PointBall: public QGraphicsEllipseItem,
                 public CollisionUpdateAffectsGame,
                 public CollisionUpdateAffectsObject

{
public:
    PointBall(QPoint a);
    ~PointBall() override = default;
    void UpdateOnCollisionGame(Game * _game) const override;
    void UpdateOnCollisionObject() override;
private:

QPoint position;

};




#endif // BALL_H
