#ifndef SYSFUNCTIONS_H
#define SYSFUNCTIONS_H
#include <QGraphicsView>
#include <QMessageBox>
#include <memory>

class SysFuctionInterface
{
public:
    virtual ~SysFuctionInterface() = default;
    virtual void showView(QGraphicsView * viewtoshow) = 0;
    virtual QList<QPair<QString,int>> ReadBestPlayers(const QString & fileadress) =0;
    virtual void SaveBestPlayers(QList<QPair<QString,int>>& BestPlayers, const QString & fileadress) = 0;
    virtual void initFinalMessage(QGraphicsView * parent = nullptr) = 0;
    virtual void setFinalMessageWin() = 0;
    virtual void setFinalMessageLoose() = 0;
    virtual void showFinalMessage() const = 0;
    virtual void GameInit() const = 0;
    virtual void MenuInit() const = 0;

};

class SysFuction: public SysFuctionInterface
{
public:
    void showView(QGraphicsView * viewtoshow) override;
    QList<QPair<QString,int>> ReadBestPlayers(const QString & fileadress) override;
    void initFinalMessage(QGraphicsView * parent = nullptr) override;
    void setFinalMessageWin() override;
    void setFinalMessageLoose() override;
    void SaveBestPlayers(QList<QPair<QString, int> > &BestPlayers, const QString & fileadress) override;
    void showFinalMessage() const override;
    void GameInit() const  override ;
    void MenuInit() const override;
private:
    std::shared_ptr<QMessageBox> FinalMessage{nullptr};
};


#endif // SYSFUNCTIONS_H
