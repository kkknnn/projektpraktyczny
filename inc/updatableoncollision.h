#ifndef UPDATABLEONCOLLISION_H
#define UPDATABLEONCOLLISION_H
#include "pacman.h"

class Game;


class  CollisionUpdateAffectsGame
{
public:
    virtual ~CollisionUpdateAffectsGame()=default;
    virtual void UpdateOnCollisionGame(Game * _game) const =0;

};
class CollisionUpdateAffectsObject
{
   public:
    virtual ~CollisionUpdateAffectsObject() = default;
    virtual void UpdateOnCollisionObject() = 0;
};

class CollisionUpdateAffectsPacman
{
public:
    virtual ~CollisionUpdateAffectsPacman() = default;
    virtual void UpdateOnCollisionPacman(pacman * _pac) const =0;
};


#endif // UPDATABLEONCOLLISION_H
