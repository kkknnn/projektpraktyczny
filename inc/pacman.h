#ifndef PACMAN_H
#define PACMAN_H

#include <QPointF>
#include <QVector>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QList>
#include <QMediaPlayer>
#include <memory>

class Game;


class PacmanInterface: public QGraphicsPixmapItem

{
public:

virtual void keyPressEvent(QKeyEvent * event) override = 0;
virtual QPoint getPosition() = 0 ;
virtual void setPosition(QPoint) = 0;
virtual void update() =0;
virtual void CollisionReact(QList<QGraphicsItem*> &) = 0;
virtual int getAnimationPhase() const = 0;
virtual void setAnimationPhase(int value) = 0;
virtual void animate() = 0;
virtual int getActualDirection() const = 0;
virtual QPoint getLastMove() const = 0;
};



class pacman: public PacmanInterface

{
public:

   pacman(Game * _game = nullptr);
   virtual ~pacman() override = default;
   void keyPressEvent(QKeyEvent * event) override;
   QPoint getPosition() override;
   void setPosition(QPoint ) override;
   void CollisionReact(QList<QGraphicsItem*> &) override;
   void setDead();
   void update() override;
   int getAnimationPhase() const override ;
   void setAnimationPhase(int value) override;
   void animate() override;
   int getActualDirection() const override;
   QPoint getLastMove() const override;
   void movePosibilityCheck(QPoint);


private:
   void setPositon(int x, int y);
   Game * pacmangame;
   int AnimationPhase;
   int ActualDirection;
   QPoint lastmove;
   bool PosibleMoveDown;
   bool PosibleMoveUp;
   bool PosibleMoveLeft;
   bool PosibleMoveRight;
   int Speed;
   int AnimationHelper;
   QPixmap up1,up2,up3;
   QPixmap right1,right2,right3;
   QPixmap down1,down2,down3;
   QPixmap left1,left2,left3;
   std::unique_ptr<QMediaPlayer> EatHyperBallSound;
   std::unique_ptr<QMediaPlayer> EatBallSound;
   std::unique_ptr<QMediaPlayer> PacmanDeadSound;
};

#endif // PACMAN_H
