#ifndef GHOST_H
#define GHOST_H
#include "pacman.h"
#include <QObject>
#include <QTimer>
#include "updatableoncollision.h"

class Game;

enum GhostType
{
    Green = 0,
    Blue = 1,
    Yellow = 2,
    Red = 3

};

class GhostInterface: public QGraphicsPixmapItem
{

public:
    virtual void animate()=0;
    virtual QPoint getTargetdestination() const =0;
    virtual void setTargetdestination(const QPoint &value)=0;
    virtual void setVelocity(const QPoint &)=0;
    virtual void ChooseNextMove() =0;
    virtual bool isWaiting() = 0;
    virtual bool isDead() const = 0;
    virtual void setDead() = 0;
    virtual void update() = 0 ;
    virtual void setScatteredModeTarget()=0;
    virtual void setChaseModeTarget()=0;
    virtual void setValnurableMode()=0;
    virtual void GoOut()=0;
    virtual void ChaseModeDirectAttack() = 0;
    virtual void ChaseModeAttackFromBehind()=0;
    virtual void ChaseModeAttackPredictedPosition()=0;
    virtual void ChaseModeGetThenRunAwayTactics()=0;

};




class ghost:  public QObject,
              public GhostInterface,
              public CollisionUpdateAffectsGame,
              public CollisionUpdateAffectsPacman,
              public CollisionUpdateAffectsObject

{
    Q_OBJECT
public:
    ghost(Game *, int ghostnumber);
    ~ghost() override = default;
    void animate() override;
    QPoint getTargetdestination() const override;
    void setTargetdestination(const QPoint &value) override;
    void setVelocity(const QPoint &) override;
    void ChooseNextMove() override;
    bool isWaiting() override;
    bool isDead()const override;
    void setDead()override;
    void ChaseModeDirectAttack()override;
    void ChaseModeAttackFromBehind()override;
    void ChaseModeAttackPredictedPosition()override;
    void ChaseModeGetThenRunAwayTactics()override;
    QPoint getNextMove() const;
    void UpdateOnCollisionPacman(pacman * _pac) const override;
    void UpdateOnCollisionGame(Game * _game) const override;
    void UpdateOnCollisionObject() override;

public slots:
      void update() override;
      void setScatteredModeTarget() override;
      void setChaseModeTarget() override;
      void setValnurableMode()override;
      void GoOut()override;
private:
    QPixmap left,right,up,down,valnurable;
    QPoint lastmove;
    QPoint nextmove;
    QPoint targetdestination;
    QTimer * ChaseSettingTarget;
    Game * pacmangame;

    bool PowerballMode;
    bool WaitMode;
    bool ScatterMode;
    bool DeadMode;
    bool ChaseMode;
    bool SpeedMayChange;
    int Animationphase;
    int AnimationHelper;
    int PhaseChanger;
    int GhostType;
    int VelocityX;
    int VelocityY;
    int Speed;
    void move();
};


#endif // GHOST_H

