#ifndef HYPERBALL_H
#define HYPERBALL_H
#include <QGraphicsRectItem>
#include "updatableoncollision.h"

class Hyperball: public QGraphicsRectItem,
                 public CollisionUpdateAffectsGame,
                 public CollisionUpdateAffectsObject

{
public:

    Hyperball(QPoint position);
    void UpdateOnCollisionGame(Game * _game) const override;
    void UpdateOnCollisionObject() override;
};

#endif // HYPERBALL_H
