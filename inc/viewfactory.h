#ifndef VIEWFACTORY_H
#define VIEWFACTORY_H

#include <memory>
#include <QGraphicsView>
#include <thread>
#include "game.h"
#include <QThread>
#include "mainmenu.h"
#include "sysfunctions.h"
#include <QVector>


enum GamePhase
{
    Menu = 1,
    PacmanGame = 2
};

class PacmanGameFactory;
class MainMenuFactory;


class ViewFactory
{
public:
    virtual ~ViewFactory() = default;
    static std::shared_ptr<ViewFactory> getViewFactory(GamePhase view);
    virtual void createView()=0;
    static QVector<std::shared_ptr<QGraphicsView>> ActualView ;

};




class PacmanGameFactory: public ViewFactory
{
public:
    virtual ~PacmanGameFactory() override = default;
    virtual void createView() override
    {
    if (ActualView.size()>0)
    {

        if(ActualView.size()>2)
            for (int i=0;i<ActualView.size()-2;i++)
            {
             ActualView[i].reset();
             ActualView.pop_front();
            }
        int iter = ActualView.size()-1;
        auto previousview = ActualView[iter].get();
        MainMenu * previousmenu = static_cast<MainMenu *> (ActualView[iter].get());
        SysFuctionInterface * _SYS = new SysFuction();
        std::shared_ptr<Game>  game = std::make_shared<Game>(previousmenu, _SYS);
        game->setGeometry(previousview->pos().x(),previousview->pos().y(),previousview->width(),previousview->height());
        game->setFocus();
        ActualView.push_back(game);
        ActualView.rbegin()->get()->setFocus();
        previousview->close();
        return ;

    }

    else
    {
        MainMenu * menu = new MainMenu();
        SysFuctionInterface * sys = new SysFuction();
        ActualView.push_back(std::make_shared<Game>(menu, sys));
        return;
    }
    }


};


class MainMenuFactory: public virtual ViewFactory
{
public:
    virtual ~MainMenuFactory() override = default;
    virtual void createView() override
    {
        if (ActualView.size()>0)
        {

            if(ActualView.size()>2)
                for (int i=0;i<ActualView.size()-2;i++)
                {
                 ActualView[i].reset();
                 ActualView.pop_front();
                }


            auto iter = ActualView.size()-1;
            auto previousview = ActualView[iter].get();
            std::shared_ptr<QGraphicsView> mmenu = std::make_shared<MainMenu>();
            mmenu->setFocus();
            mmenu->setGeometry(previousview->pos().x(),previousview->pos().y(),previousview->width(),previousview->height());
            ActualView.push_back(mmenu);
            return;
        }

        else
        {
            ActualView.push_back(std::make_shared<MainMenu>());
            return;
        }
        }
};


#endif // VIEWFACTORY_H

