#ifndef BOARD_H
#define BOARD_H
#include <QVector>
#include <QSet>
#include <QPointF>
#include <set>


class BoardInterface
{
public:
     virtual ~BoardInterface()=default;
     virtual QVector<QPoint> getMoveVector(QPoint &) = 0;
     virtual void addMoveVector(QVector<QPoint> &) =0;
     virtual bool checkIfCrossroad(QPoint &) = 0;
     virtual QVector<QPoint> getPointBallLocations() const = 0;
     virtual QPoint getPointballSingleLocation(int number) const = 0;
     virtual QVector<QPoint> getAllCrossroads() = 0;
};



class PacmanBoard:public BoardInterface
{
public:
    PacmanBoard();
    QVector<QPoint> getMoveVector(QPoint &);
    void addMoveVector(QVector<QPoint> &);
    QVector<QPoint> getAllCrossroads();
    bool checkIfCrossroad(QPoint &);
    QVector<QPoint> getPointBallLocations() const;
    QPoint getPointballSingleLocation(int number) const;

private:
    QVector<QVector<QPoint>> crossroads;
    QVector<QPoint> PointBallLocations;




};



#endif // BOARD_H
