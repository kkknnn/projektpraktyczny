#ifndef GLOBALVARIABLES_H
#define GLOBALVARIABLES_H
#include <QList>

class GlobalVariables
{
public:
    static QList<QPair<QString,int>> TopPlayers;
    static QString PlayerName;

};


#endif // GLOBALVARIABLES_H
