#pragma once
#include <QGraphicsView>
#include <QWidget>
#include <QLabel>
#include <QGraphicsScene>
#include "pacman.h"
#include "board.h"
#include "mainmenu.h"
#include "ghost.h"
#include "sysfunctions.h"


class GameInterface
{
public:
    virtual ~GameInterface() = default;
    virtual bool isPowerBallMode() = 0;
    virtual bool addObject(QGraphicsItem *) = 0;
    virtual QPoint getPacmanPosition() = 0;
    virtual QPoint getPacmanLastMove() = 0;
    virtual int getPacmanDirection() = 0;
    virtual int getScore() const = 0;
    virtual int getHealth() const = 0;
    virtual QVector<GhostInterface *> getAllGhosts() const = 0;
    virtual void decreaseHealth() = 0;
    virtual void increaseScore()=0;
    virtual void GhostRelease()=0;
    virtual void UpdateAllObjects() = 0;
    virtual void PowerBallActivation() =0;
};


class Game: public QGraphicsView, public GameInterface
{
    Q_OBJECT
public:
    Game(MainMenuInterface *, SysFuctionInterface *, QWidget * parent=nullptr);
    Game(QVector<GhostInterface*>, PacmanInterface *, BoardInterface *, SysFuctionInterface *, MainMenuInterface*, QWidget * parent=nullptr);
    virtual ~Game();
    bool addObject(QGraphicsItem *);
    BoardInterface * board;
    QPoint getPacmanPosition();
    QPoint getPacmanLastMove();
    QVector<GhostInterface *> getAllGhosts() const;
    int getPacmanDirection();
    bool isPowerBallMode();
    int getScore() const;
    void decreaseHealth();
    void increaseScore();
    int getHealth() const;
    void ActualiseTopPlayers();

public slots:
    void GhostRelease();
    void UpdateAllObjects();
    void PowerBallActivation();

private:
    void GameOver();
    int GoOutSequencer;
    PacmanInterface * player;
    QVector<GhostInterface*> GhostArray;
    QGraphicsScene * PacmanGame;
    std::shared_ptr<MainMenuInterface> Menu;
    QTimer * GhostMoveTimer;
    QTimer * GhostMoveOut;
    QTimer * PowerballMode;
    QLabel * ScoreLabel;
    QLabel * LifeLabel;
    std::shared_ptr<QMediaPlayer> BackgroundMusic;
    std::shared_ptr<QMediaPlayer> PowerballModeMusic;
    std::shared_ptr<QMediaPlaylist> BackgroundSound;
    int Score;
    int Health;
    std::shared_ptr<SysFuctionInterface> System;
    bool powerballmode;
};

