#include <QApplication>
#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QTimer>
#include <QDebug>
#include "inc/game.h"
#include <iostream>
#include "inc/mainmenu.h"
#include "inc/viewfactory.h"

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    auto Main = ViewFactory::getViewFactory(GamePhase::Menu);
    Main->createView();
    a.exec();
    for (int i = 0; i < ViewFactory::ActualView.size();i++)
    {
    ViewFactory::ActualView[i].reset();
    }
    return 0;
}
