QT += gui\
      multimedia \
      core\
      widgets

CONFIG += c++11 console
CONFIG += c++14
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

#precompile_header:!isEmpty(PRECOMPILED_HEADER) {
#DEFINES += USING_PCH
#}
# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
    src/game.cpp \
    src/pacman.cpp \
    src/board.cpp \
    src/ball.cpp \
    src/mainmenu.cpp \
    src/ghost.cpp \
    src/hyperball.cpp \
    src/viewfactory.cpp \
    src/sysfunctions.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    inc/game.h \
    inc/pacman.h \
    inc/board.h \
    inc/ball.h \
    inc/mainmenu.h \
    inc/ghost.h \
    inc/hyperball.h \
    inc/viewfactory.h \
    inc/sysfunctions.h \
    inc/updatableoncollision.h

RESOURCES += \
    res.qrc
