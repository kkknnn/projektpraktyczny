#pragma once
#include <QGraphicsProxyWidget>
#include <QPixmap>
#include <QtTest/QtTest>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>
#include "mocks/mock_game.h"
#include "mocks/mock_ghost.h"
#include "mocks/mock_packman.h"
#include "mocks/mock_pacmanboard.h"
#include "mocks/mock_sysfunc.h"
#include "mocks/mock_mainmenu.h"
#include "../../inc/ball.h"
#include "../../inc/sysfunctions.h"
#include "../../inc/ghost.h"
#include "../../inc/hyperball.h"

using namespace testing;

class GameTests: public Test
{
protected:
    NiceMock<mock_pacmanboard> m_board{};
    NiceMock<mock_sysfunc> m_func{};
    NiceMock<mock_mainmenu>m_menu{};
    mock_ghost * ghost0;
    mock_ghost * ghost1;
    mock_ghost * ghost2;
    mock_ghost * ghost3;
    mock_pacman * m_pacman;

    Game * game;
    GameTests(){
         ghost0 = new mock_ghost();
         ghost1 = new mock_ghost();
         ghost2 = new mock_ghost();
         ghost3 = new mock_ghost();
         QVector<GhostInterface*> m_ghost_vector {ghost0,ghost1,ghost2,ghost3};
         m_pacman=new mock_pacman();
         game = new Game(m_ghost_vector,m_pacman,&m_board,&m_func,&m_menu);


    }
    ~GameTests()
    {
        delete m_pacman;
        delete ghost0;
        delete ghost1;
        delete ghost2;
        delete ghost3;


    }

};

TEST(GameInitTests,ProperPacmanPositionOnStart)
{

    NiceMock<mock_sysfunc> * m_sys = new  NiceMock<mock_sysfunc>;
    NiceMock<mock_mainmenu>* m_men = new NiceMock<mock_mainmenu>;

    Game * game = new Game(m_men, m_sys);

    QPoint ExpectedPosition(0,240);
    QPoint PointReturned = game->getPacmanPosition();

    EXPECT_EQ(ExpectedPosition,PointReturned);
    delete game;
}

TEST(GameInitTests,AllObjectCreatedAndAddedToScene)
{

    NiceMock<mock_sysfunc> m_sys;
    NiceMock<mock_mainmenu> m_men;

    Game * game = new Game(&m_men, &m_sys);

    auto vector = game->scene()->items();
    EXPECT_EQ(204,vector.size());

}
TEST(GameInitTests,PacmanCreatedAndAddedToScene)
{


    NiceMock<mock_sysfunc> m_sys;
    NiceMock<mock_mainmenu> m_men;

    Game * game = new Game(&m_men, &m_sys);

    auto vector = game->scene()->items();
    std::vector<pacman*> pacmanobjects;
    for (int i=0;i<vector.size();i++)
    {
        pacman * posibleplayer = dynamic_cast<pacman*>(vector[i]);
        if (posibleplayer!=nullptr)
        {

            pacmanobjects.push_back(posibleplayer);


        }
    }
    EXPECT_EQ(1,pacmanobjects.size());

}

TEST(GameInitTests,BallsCreatedAndAddedToScene)
{


    NiceMock<mock_sysfunc> m_sys;
    NiceMock<mock_mainmenu> m_men;
    Game * game = new Game(&m_men, &m_sys);
    auto vector = game->scene()->items();
    std::vector<PointBall*> ballsobjects;
    for (int i=0;i<vector.size();i++)
    {
        PointBall * posibleobject = dynamic_cast<PointBall*>(vector[i]);
        if (posibleobject!=nullptr)
        {

            ballsobjects.push_back(posibleobject);


        }

    }

    EXPECT_EQ(190,ballsobjects.size());
}

TEST(GameInitTests,PowerBallsCreatedAndAddedToScene)
{


    NiceMock<mock_sysfunc> m_sys;
    NiceMock<mock_mainmenu> m_men;
    Game * game = new Game(&m_men, &m_sys);
    auto vector = game->scene()->items();
    std::vector<Hyperball*> ballsobjects;
    for (int i=0;i<vector.size();i++)
    {
        Hyperball * posibleobject = dynamic_cast<Hyperball*>(vector[i]);
        if (posibleobject!=nullptr)
        {

            ballsobjects.push_back(posibleobject);


        }

    }

    EXPECT_EQ(4,ballsobjects.size());

}


TEST(GameInitTests,GhostsCreatedAndAddedToScene)
{


    NiceMock<mock_sysfunc> m_sys;
    NiceMock<mock_mainmenu> m_men;

    Game * game = new Game(&m_men, &m_sys);
    auto vector = game->scene()->items();
    std::vector<ghost*> ballsobjects;
    for (int i=0;i<vector.size();i++)
    {
        ghost * posibleobject = dynamic_cast<ghost*>(vector[i]);
        if (posibleobject!=nullptr)
        {

            ballsobjects.push_back(posibleobject);


        }

    }

    EXPECT_EQ(4,ballsobjects.size());
}

TEST(GameInitTests,ScoreAndHealthLabelsCreatedAndAddedToScene)
{


    NiceMock<mock_sysfunc> m_sys;
    NiceMock<mock_mainmenu> m_men;
    Game * game = new Game(&m_men, &m_sys);
    auto vector = game->scene()->items();
    std::vector<QGraphicsProxyWidget *> widget;
    for (int i=0;i<vector.size();i++)
    {
        QGraphicsProxyWidget  * posibleobject = dynamic_cast<QGraphicsProxyWidget*>(vector[i]);
        if (posibleobject!=nullptr)
        {

            widget.push_back(posibleobject);


        }

    }

    EXPECT_EQ(2,widget.size());
}



TEST(GameInitTests,ProperPacmanLastMoveOnStart)
{


    NiceMock<mock_sysfunc> m_sys;
    NiceMock<mock_mainmenu> m_men;

    Game * game = new Game(&m_men, &m_sys);

    QPoint ExpectedPosition(-24,240);
    QPoint PointReturned = game->getPacmanLastMove();

    EXPECT_EQ(ExpectedPosition,PointReturned);

}


TEST(GameInitTests,ProperGhostsPositionOnStart)
{


    NiceMock<mock_sysfunc> m_sys;
    NiceMock<mock_mainmenu> m_men;

    Game * game = new Game(&m_men, &m_sys);

    QPoint ExpectedPosition(216,240);
    auto ghosts = game->getAllGhosts();

    for(int i=0;i<ghosts.size();i++)
    {
    EXPECT_EQ(ExpectedPosition,ghosts[i]->pos());
    }

}

TEST(GameInitTests,GhostAreCreated)
{


    NiceMock<mock_sysfunc> m_sys;
    NiceMock<mock_mainmenu> m_men;
    Game * game = new Game(&m_men, &m_sys);

    auto ghosts = game->getAllGhosts();

    for(int i=0;i<ghosts.size();i++)
    {
    EXPECT_NE(nullptr,ghosts[i]);
    }
    EXPECT_EQ(4,ghosts.size());

}



TEST(GameClassTests,IncreasingScoreChangesValueBy1)
{

    NiceMock<mock_sysfunc> m_sys;
    NiceMock<mock_mainmenu> m_men;

    Game * game = new Game(&m_men, &m_sys);

    int PreviousScore=game->getScore();
    game->increaseScore();

    EXPECT_EQ(PreviousScore+1,game->getScore());

}


TEST(GameClassTests,AddingNullptrObjectReturnsFalse)
{

    NiceMock<mock_sysfunc> m_sys;
    NiceMock<mock_mainmenu> m_men;
    Game * game = new Game(&m_men, &m_sys);
    QGraphicsPixmapItem * ItemToAdd = nullptr;

    EXPECT_FALSE(game->addObject(ItemToAdd));

}


TEST(GameClassTests,ChangingToPowerballModeAndBackToNormal)
{

    NiceMock<mock_sysfunc> m_sys;
    NiceMock<mock_mainmenu> m_men;
    Game * game = new Game(&m_men, &m_sys);
    EXPECT_FALSE(game->isPowerBallMode());
    game->PowerBallActivation();
    EXPECT_TRUE(game->isPowerBallMode());
    game->PowerBallActivation();
    EXPECT_FALSE(game->isPowerBallMode());

}



TEST(GameClassTests,AddingObjectFunction)
{

    NiceMock<mock_sysfunc> m_sys;
    NiceMock<mock_mainmenu> m_men;
    Game * game = new Game(&m_men, &m_sys);

    int PreviousSceneItemsSize=game->scene()->items().size();
    QGraphicsPixmapItem * ItemToAdd = new QGraphicsPixmapItem();

    game->addObject(ItemToAdd);

    EXPECT_EQ(PreviousSceneItemsSize+1,game->scene()->items().size());

}


TEST(GameClassTests,AllGhostCanBeReleased)
{

    NiceMock<mock_sysfunc> m_sys;
    NiceMock<mock_mainmenu> m_men;
    Game * game = new Game(&m_men, &m_sys);
    auto ghosts = game->getAllGhosts();


    for(int i=0;i<ghosts.size();i++)
    {
    game->GhostRelease();
    }

    EXPECT_FALSE(ghosts[0]->isWaiting());
    EXPECT_FALSE(ghosts[1]->isWaiting());
    EXPECT_FALSE(ghosts[2]->isWaiting());
    EXPECT_FALSE(ghosts[3]->isWaiting());

}

TEST(GameClassTests, GettingPacmanRealDirection)
{

  NiceMock<mock_sysfunc> m_sys;
  NiceMock<mock_mainmenu> m_men;
  mock_pacman * m_pacman = new mock_pacman();
  NiceMock<mock_pacmanboard> m_board;
  mock_ghost * ghost0 = new mock_ghost();
  mock_ghost * ghost1 = new mock_ghost();
  mock_ghost * ghost2 = new mock_ghost();
  mock_ghost * ghost3 = new mock_ghost();

  QVector<GhostInterface*> m_ghost_vector{ghost0,ghost1,ghost2,ghost3};
  EXPECT_CALL(*m_pacman,getActualDirection()).WillRepeatedly(Return(1));

  Game * game = new Game(m_ghost_vector,m_pacman,&m_board,&m_sys, &m_men);
  game->getPacmanDirection();
  delete m_pacman;
  delete ghost0; delete ghost1 ; delete ghost2 ; delete ghost3;
};


TEST(GameClassTests,DeacreasingHealthChangesValueBy1)
{
    NiceMock<mock_sysfunc> m_sys;
    NiceMock<mock_mainmenu> m_men;
    Game * game = new Game(&m_men, &m_sys);
    int PreviousHealth=game->getHealth();
    game->decreaseHealth();

    EXPECT_EQ(PreviousHealth-1,game->getHealth());



}

TEST_F(GameTests,GettingPacmanRealDirection)
{

    EXPECT_CALL(*m_pacman,getActualDirection());
    game->getPacmanDirection();


}

TEST_F(GameTests,GameOverTestNoPlayersOnList)
{

EXPECT_CALL(m_func,initFinalMessage(_));
EXPECT_CALL(m_func,setFinalMessageWin());
EXPECT_CALL(m_func,showFinalMessage());
EXPECT_CALL(m_menu,getTopPlayers()).Times(2).WillRepeatedly(Return(QList<QPair<QString, int>>{}));
EXPECT_CALL(m_menu,getPlayerNickname());

game->ActualiseTopPlayers();

}
TEST_F(GameTests,GameOverAfterReaching189points)
{
    EXPECT_CALL(m_func,initFinalMessage(_));
    EXPECT_CALL(m_func,setFinalMessageWin());
    EXPECT_CALL(m_func,showFinalMessage());
    EXPECT_CALL(m_menu,getTopPlayers()).Times(2).WillRepeatedly(Return(QList<QPair<QString, int>>{{"",30},{"",20},{"",20},{"",20},{"",-20}}));
    EXPECT_CALL(m_menu,getPlayerNickname());
    EXPECT_CALL(m_func,MenuInit());
for(int i=0;i<190;i++)
{
  game->increaseScore();
}


}

TEST(MainMenuTests, ProperInitialization)
{
    NiceMock<mock_sysfunc> * m_sys = new  NiceMock<mock_sysfunc>;
    EXPECT_CALL(*m_sys,showView(_));
    EXPECT_CALL(*m_sys,ReadBestPlayers(_));
    MainMenu * menu = new MainMenu(m_sys);
    delete menu;

}

TEST(MainMenuTests, GettinReadedValueOfBestPlayerList)
{
    NiceMock<mock_sysfunc> * m_sys = new  NiceMock<mock_sysfunc>;
    EXPECT_CALL(*m_sys,ReadBestPlayers(_)).Times(1).WillOnce(Return(QList<QPair<QString, int>>{{"",30},{"",20}}));
    MainMenu * menu = new MainMenu(m_sys);
    auto ExpectedValue = QList<QPair<QString, int>>{{"",30},{"",20}};
    auto ReturnedValue = menu->getTopPlayers();
    EXPECT_EQ(ExpectedValue,ReturnedValue);
    delete menu;

}

TEST(MainMenuTests, AddingSceneElements)
{
    MainMenu * menu = new MainMenu();
    EXPECT_EQ(6,menu->scene()->items().size());
    delete menu;

}
TEST(MainMenuTests, GameInitialization)
{
    NiceMock<mock_sysfunc> * m_sys = new  NiceMock<mock_sysfunc>;
    MainMenu * menu = new MainMenu(m_sys);
    EXPECT_CALL(*m_sys,GameInit());
    menu->GameInit();
    delete menu;

}
TEST(MainMenuTests, PlayerNameSetsAfterStartAndReturnProperValue)
{
    NiceMock<mock_sysfunc> * m_sys = new  NiceMock<mock_sysfunc>;
    MainMenu * menu = new MainMenu(m_sys);
    EXPECT_EQ("",menu->getPlayerNickname());
    menu->GameInit();
    EXPECT_EQ("Enter your nickname",menu->getPlayerNickname());

    delete menu;

}


TEST_F(GameTests,GameOverTestQualifiedToBestPlayers)
{

EXPECT_CALL(m_func,initFinalMessage(_));
EXPECT_CALL(m_func,setFinalMessageWin());
EXPECT_CALL(m_func,showFinalMessage());
EXPECT_CALL(m_menu,getTopPlayers()).Times(2).WillRepeatedly(Return(QList<QPair<QString, int>>{{"",30},{"",20},{"",20},{"",20},{"",-20}}));
EXPECT_CALL(m_menu,getPlayerNickname());

game->ActualiseTopPlayers();

}



TEST_F(GameTests,GameOverTestNotQualifiedToBestPlayers)
{

EXPECT_CALL(m_func,initFinalMessage(_));
EXPECT_CALL(m_func,setFinalMessageLoose());
EXPECT_CALL(m_func,showFinalMessage());
EXPECT_CALL(m_menu,getTopPlayers()).Times(2).WillRepeatedly(Return(QList<QPair<QString, int>>{{"",30},{"",20},{"",20},{"",20},{"",15}}));
EXPECT_CALL(m_menu,getPlayerNickname());

game->ActualiseTopPlayers();

}




TEST_F(GameTests,GettingPacmanRealLastMove)
{

    EXPECT_CALL(*m_pacman,getLastMove());
    game->getPacmanLastMove();


}
TEST_F(GameTests, UpdateSlotUpdatesAllObjects)
{

    EXPECT_CALL(*m_pacman,update()).Times(Exactly(1));
    EXPECT_CALL(*ghost0,update()).Times(Exactly(1));
    EXPECT_CALL(*ghost1,update()).Times(Exactly(1));
    EXPECT_CALL(*ghost2,update()).Times(Exactly(1));
    EXPECT_CALL(*ghost3,update()).Times(Exactly(1));
    game->UpdateAllObjects();

}

TEST_F(GameTests, PowerBallModeAffectsAllGhostAndSetsTheProperMode)
{

    EXPECT_CALL(*ghost0,setValnurableMode()).Times(Exactly(1));
    EXPECT_CALL(*ghost1,setValnurableMode()).Times(Exactly(1));
    EXPECT_CALL(*ghost2,setValnurableMode()).Times(Exactly(1));
    EXPECT_CALL(*ghost3,setValnurableMode()).Times(Exactly(1));

    EXPECT_EQ(false,game->isPowerBallMode());
    game->PowerBallActivation();
    EXPECT_EQ(true,game->isPowerBallMode());

}

TEST_F(GameTests, GameCanReleaseGhosts)
{
    EXPECT_CALL(*ghost0,isWaiting()).Times(Exactly(1)).WillRepeatedly(Return(true));
    EXPECT_CALL(*ghost1,isWaiting()).Times(Exactly(1)).WillRepeatedly(Return(true));
    EXPECT_CALL(*ghost2,isWaiting()).Times(Exactly(1)).WillRepeatedly(Return(true));
    EXPECT_CALL(*ghost3,isWaiting()).Times(Exactly(1)).WillRepeatedly(Return(true));


    EXPECT_CALL(*ghost0,GoOut()).Times(Exactly(1));
    EXPECT_CALL(*ghost1,GoOut()).Times(Exactly(1));
    EXPECT_CALL(*ghost2,GoOut()).Times(Exactly(1));
    EXPECT_CALL(*ghost3,GoOut()).Times(Exactly(1));

    auto ghosts = game->getAllGhosts();

    for(int i=0;i<ghosts.size();i++)
    {
        game->GhostRelease();
    }

}

TEST_F(GameTests, GameChecksIfGhostIsWaintingBeforeRelease)
{

    EXPECT_CALL(*ghost0,isWaiting()).Times(Exactly(4));
    EXPECT_CALL(*ghost1,isWaiting()).Times(Exactly(4));
    EXPECT_CALL(*ghost2,isWaiting()).Times(Exactly(4));
    EXPECT_CALL(*ghost3,isWaiting()).Times(Exactly(4));

    auto ghosts = game->getAllGhosts();

    for(int i=0;i<ghosts.size();i++)
    {
        game->GhostRelease();
    }

}



TEST_F(GameTests, GameReleaseWorksProperly)
{

    EXPECT_CALL(*ghost0,isWaiting()).WillOnce(Return(true)).WillRepeatedly(Return(false));
    EXPECT_CALL(*ghost1,isWaiting()).WillOnce(Return(true)).WillRepeatedly(Return(false));
    EXPECT_CALL(*ghost2,isWaiting()).WillOnce(Return(true)).WillRepeatedly(Return(false));
    EXPECT_CALL(*ghost3,isWaiting()).WillOnce(Return(true)).WillRepeatedly(Return(false));
    EXPECT_CALL(*ghost0,GoOut()).Times(Exactly(1));
    EXPECT_CALL(*ghost1,GoOut()).Times(Exactly(1));
    EXPECT_CALL(*ghost2,GoOut()).Times(Exactly(1));
    EXPECT_CALL(*ghost3,GoOut()).Times(Exactly(1));

    auto ghosts = game->getAllGhosts();

    for(int i=0;i<ghosts.size();i++)
    {
        game->GhostRelease();
    }

}


TEST_F(GameTests, GameReleaseWorksProperlyWhileAllGhostReleased)
{

    EXPECT_CALL(*ghost0,isWaiting()).Times(AtLeast(2)).WillOnce(Return(true)).WillRepeatedly(Return(false));
    EXPECT_CALL(*ghost1,isWaiting()).Times(AtLeast(2)).WillOnce(Return(true)).WillRepeatedly(Return(false));
    EXPECT_CALL(*ghost2,isWaiting()).Times(AtLeast(2)).WillOnce(Return(true)).WillRepeatedly(Return(false));
    EXPECT_CALL(*ghost3,isWaiting()).Times(AtLeast(2)).WillOnce(Return(true)).WillRepeatedly(Return(false));
    EXPECT_CALL(*ghost0,GoOut()).Times(Exactly(1));
    EXPECT_CALL(*ghost1,GoOut()).Times(Exactly(1));
    EXPECT_CALL(*ghost2,GoOut()).Times(Exactly(1));
    EXPECT_CALL(*ghost3,GoOut()).Times(Exactly(1));

    auto ghosts = game->getAllGhosts();

    for(int i=0;i<ghosts.size();i++)
    {
        game->GhostRelease();
    }
    game->GhostRelease();

}


class PacmanTests : public Test
{
    public:

    PacmanTests()
    {
        m_game=new mock_game(&m_sys,&m_men);
        player=new pacman(m_game);

    }

    ~PacmanTests()
    {
        delete player;

    }

protected:
    NiceMock<mock_sysfunc> m_sys{};
    NiceMock<mock_mainmenu> m_men{};
    mock_game * m_game;
    pacman * player;

    void SetUp()
    {
        player->setFocus();
    }

};

class PacmanTestsMockedGame : public Test
{
    public:
    PacmanTestsMockedGame()
    {   ghost0 = new mock_ghost();
        ghost1 = new mock_ghost();
        ghost2 = new mock_ghost();
        ghost3 = new mock_ghost();
        QVector<GhostInterface*> m_ghost_vector {ghost0,ghost1,ghost2,ghost3};
        m_pacman=new mock_pacman();
        game = new Game(m_ghost_vector,m_pacman,&m_board,&m_sys, &m_men);
        player = new pacman (game);
    }

    ~PacmanTestsMockedGame()
    {

        delete player;

    }

protected:
    pacman * player;
    Game * game;
    NiceMock<mock_pacmanboard> m_board;
    NiceMock<mock_sysfunc> m_sys;
    NiceMock<mock_mainmenu> m_men;
    mock_ghost * ghost0;
    mock_ghost * ghost1;
    mock_ghost * ghost2;
    mock_ghost * ghost3;
    mock_pacman * m_pacman;
};





TEST_F(PacmanTests, PacmanInitialPosition)
{
    EXPECT_EQ(player->getPosition(),QPoint(0,240));

}

TEST_F(PacmanTests, PacmanCanMoveRight)
{
    QKeyEvent * event = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Right,Qt::NoModifier);
    EXPECT_EQ(player->getPosition(),QPoint(0,240));
    player->keyPressEvent(event);
    EXPECT_EQ(player->getPosition(),QPoint(8,240));
    player->keyPressEvent(event);
    EXPECT_EQ(player->getPosition(),QPoint(16,240));
    delete event;

}


TEST_F(PacmanTests, PacmanCanMoveLeft)
{
    QKeyEvent * event = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Left,Qt::NoModifier);
    EXPECT_EQ(player->getPosition(),QPoint(0,240));
    player->keyPressEvent(event);
    EXPECT_EQ(player->getPosition(),QPoint(-8,240));
    player->keyPressEvent(event);
    EXPECT_EQ(player->getPosition(),QPoint(-16,240));
    delete event;

}

TEST_F(PacmanTests, PacmanCantMoveUpOrDownOnStart)
{
    QKeyEvent * event = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Up,Qt::NoModifier);
    QKeyEvent * event2 = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Down,Qt::NoModifier);

    EXPECT_EQ(player->getPosition(),QPoint(0,240));
    player->keyPressEvent(event);
    EXPECT_EQ(player->getPosition(),QPoint(0,240));
    player->keyPressEvent(event2);
    EXPECT_EQ(player->getPosition(),QPoint(0,240));
    delete event;
    delete event2;

}
TEST_F(PacmanTests, ChangingPosition)
{
    QPoint ExpectedPositionOnStart = QPoint(0,240);
    EXPECT_EQ(player->getPosition(),ExpectedPositionOnStart);
    player->setPosition(QPoint(240,240));
    EXPECT_EQ(player->getPosition(),QPoint(240,240));
}
TEST_F(PacmanTests, AnimationPhaseAffectsPixmap)
{
    QPixmap  right3=QPixmap(":/img/img/pacman_pics/pacman pics/pacright3.png");
    EXPECT_EQ(player->getAnimationPhase(),0);
    player->setAnimationPhase(2);
    player->animate();
    EXPECT_EQ(player->pixmap(),right3);
}

TEST_F(PacmanTests, AnimationChange)
{
    EXPECT_EQ(player->getAnimationPhase(),0);
    player->setAnimationPhase(2);
    EXPECT_EQ(player->getAnimationPhase(),2);
}

TEST_F(PacmanTestsMockedGame, AnimationChangePixmaps)
{
  QPixmap up1=QPixmap(":/img/img/pacman_pics/pacman pics/pacup1.png");
  QPixmap  up2=QPixmap(":/img/img/pacman_pics/pacman pics/pacup2.png");
  QPixmap  up3=QPixmap(":/img/img/pacman_pics/pacman pics/pacup3.png");
  QPixmap  down1=QPixmap(":/img/img/pacman_pics/pacman pics/pacdown1.png");
  QPixmap  down2=QPixmap(":/img/img/pacman_pics/pacman pics/pacdown2.png");
  QPixmap  down3=QPixmap(":/img/img/pacman_pics/pacman pics/pacdown3.png");
  QPixmap  left1=QPixmap(":/img/img/pacman_pics/pacman pics/pacleft1.png");
  QPixmap  left2=QPixmap(":/img/img/pacman_pics/pacman pics/pacleft2.png");
  QPixmap  left3=QPixmap(":/img/img/pacman_pics/pacman pics/pacleft3.png");
  QPixmap  right1=QPixmap(":/img/img/pacman_pics/pacman pics/pacright1.png");
  QPixmap  right2=QPixmap(":/img/img/pacman_pics/pacman pics/pacright2.png");
  QPixmap  right3=QPixmap(":/img/img/pacman_pics/pacman pics/pacright3.png");

  EXPECT_FALSE(player->pixmap().isNull());
  EXPECT_EQ(player->pixmap(),right1);
  player->animate();
  player->animate();
  EXPECT_EQ(player->pixmap(),right2);
  player->animate();
  EXPECT_EQ(player->pixmap(),right3);
  QKeyEvent * eventmoveup = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Up,Qt::NoModifier);
  QKeyEvent * eventmovedown = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Down,Qt::NoModifier);
  QKeyEvent * eventmoveleft = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Left,Qt::NoModifier);
  EXPECT_CALL(m_board,checkIfCrossroad(_)).WillRepeatedly(Return(true));
  QVector<QPoint> AllSidesCrossroadVector{QPoint(0,240),QPoint(-24,240), QPoint(24,240),QPoint(0,200),QPoint(0,280)};
  EXPECT_CALL(m_board,getMoveVector(_)).WillRepeatedly(Return(AllSidesCrossroadVector));
  player->movePosibilityCheck(QPoint(0,240));
  player->keyPressEvent(eventmoveup);
  player->animate();
  EXPECT_EQ(player->pixmap(),up1);
  player->animate();
  EXPECT_EQ(player->pixmap(),up2);
  player->animate();
  EXPECT_EQ(player->pixmap(),up3);
  player->animate();
  EXPECT_EQ(player->pixmap(),up2);
  player->setPosition(QPoint(0,240));
  player->movePosibilityCheck(QPoint(0,240));
  player->keyPressEvent(eventmovedown);
  player->animate();
  EXPECT_EQ(player->pixmap(),down2);
  player->animate();
  EXPECT_EQ(player->pixmap(),down3);
  player->animate();
  EXPECT_EQ(player->pixmap(),down2);
  player->animate();
  EXPECT_EQ(player->pixmap(),down1);
  player->setPosition(QPoint(0,240));
  player->movePosibilityCheck(QPoint(0,240));
  player->keyPressEvent(eventmoveleft);
  player->animate();
  EXPECT_EQ(player->pixmap(),left3);
  player->animate();
  EXPECT_EQ(player->pixmap(),left2);
  player->animate();
  EXPECT_EQ(player->pixmap(),left1);
  player->animate();
  EXPECT_EQ(player->pixmap(),left2);

  delete eventmoveup;
  delete eventmovedown;
  delete eventmoveleft;

}


TEST_F(PacmanTestsMockedGame,DirectionIsChanging)
{
    QKeyEvent * eventmoveup = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Up,Qt::NoModifier);
    QKeyEvent * eventmovedown = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Down,Qt::NoModifier);
    QKeyEvent * eventmoveleft = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Left,Qt::NoModifier);
    EXPECT_CALL(m_board,checkIfCrossroad(_)).WillRepeatedly(Return(true));
    QVector<QPoint> AllSidesCrossroadVector{QPoint(0,240),QPoint(-24,240), QPoint(24,240),QPoint(0,200),QPoint(0,280)};
    EXPECT_CALL(m_board,getMoveVector(_)).WillRepeatedly(Return(AllSidesCrossroadVector));
    QPoint BasePosition(0,240);
    EXPECT_EQ(player->getActualDirection(),0);
    player->movePosibilityCheck(BasePosition);
    player->keyPressEvent(eventmoveup);
    EXPECT_EQ(player->getActualDirection(),3);
    player->setPosition(BasePosition);
    player->movePosibilityCheck(BasePosition);
    player->keyPressEvent(eventmovedown);
    EXPECT_EQ(player->getActualDirection(),1);
    player->setPosition(BasePosition);
    player->movePosibilityCheck(BasePosition);
    player->keyPressEvent(eventmoveleft);
    EXPECT_EQ(player->getActualDirection(),2);
    delete eventmoveup;
    delete eventmovedown;
    delete eventmoveleft;
}

TEST_F(PacmanTestsMockedGame,MovePosibilityChecksNoMove)
{

    QKeyEvent * eventmoveup = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Up,Qt::NoModifier);
    QKeyEvent * eventmovedown = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Down,Qt::NoModifier);
    QKeyEvent * eventmoveleft = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Left,Qt::NoModifier);
    QKeyEvent * eventright = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Right,Qt::NoModifier);

    EXPECT_CALL(m_board,checkIfCrossroad(_)).WillRepeatedly(Return(true));
    QVector<QPoint> NoMoveCrossroadVector{QPoint(0,240),QPoint(0,240), QPoint(0,240),QPoint(0,240),QPoint(0,240)};
    EXPECT_CALL(m_board,getMoveVector(_)).WillRepeatedly(Return(NoMoveCrossroadVector));
    player->movePosibilityCheck(player->getPosition());
    ASSERT_EQ(player->getPosition(),QPoint(0,240));

    player->keyPressEvent(eventmoveup);
    ASSERT_EQ(player->getPosition(),QPoint(0,240));

     player->keyPressEvent(eventmovedown);
     ASSERT_EQ(player->getPosition(),QPoint(0,240));
     player->keyPressEvent(eventright);
     ASSERT_EQ(player->getPosition(),QPoint(0,240));
     player->keyPressEvent(eventmoveleft);
     ASSERT_EQ(player->getPosition(),QPoint(0,240));





     delete eventmoveup;
     delete eventmovedown;
     delete eventmoveleft;
     delete eventright;

}
TEST_F(PacmanTestsMockedGame,PassageLeftWorks)
{

    QKeyEvent * eventmoveleft = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Left,Qt::NoModifier);
    EXPECT_EQ(player->getPosition(),QPoint(0,240));

    player->keyPressEvent(eventmoveleft);
    player->keyPressEvent(eventmoveleft);
    player->keyPressEvent(eventmoveleft);



    qDebug()<< player->getPosition().x() <<  player->getPosition().y();
    EXPECT_EQ(player->getPosition(),QPoint(456,240));
    delete eventmoveleft;
}
TEST_F(PacmanTestsMockedGame,PassageRightWorks)
{

    QKeyEvent * eventmoveright = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Right,Qt::NoModifier);
    EXPECT_EQ(player->getPosition(),QPoint(0,240));
    player->setPosition(QPoint(448,240));
    player->keyPressEvent(eventmoveright);
    EXPECT_EQ(player->getPosition(),QPoint(-24,240));
    delete eventmoveright;
}

TEST_F(PacmanTestsMockedGame,MovePosibilityChecksAllMovesMoveUp)
{

    QKeyEvent * eventmoveup = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Up,Qt::NoModifier);

    EXPECT_CALL(m_board,checkIfCrossroad(_)).WillRepeatedly(Return(true));
    QVector<QPoint> AllSidesCrossroadVector{QPoint(0,240),QPoint(-24,240), QPoint(24,240),QPoint(0,200),QPoint(0,280)};
    EXPECT_CALL(m_board,getMoveVector(_)).WillRepeatedly(Return(AllSidesCrossroadVector));
    player->movePosibilityCheck(player->getPosition());
    ASSERT_EQ(player->getPosition(),QPoint(0,240));
     player->keyPressEvent(eventmoveup);
     ASSERT_EQ(player->getPosition(),QPoint(0,232));

    delete eventmoveup;


}
TEST_F(PacmanTestsMockedGame,MovePosibilityChecksAllMovesMoveDown)
{

    QKeyEvent * eventmovedown = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Down,Qt::NoModifier);

    EXPECT_CALL(m_board,checkIfCrossroad(_)).WillRepeatedly(Return(true));
    QVector<QPoint> AllSidesCrossroadVector{QPoint(0,240),QPoint(-24,240), QPoint(24,240),QPoint(0,200),QPoint(0,280)};
    EXPECT_CALL(m_board,getMoveVector(_)).WillRepeatedly(Return(AllSidesCrossroadVector));
    player->movePosibilityCheck(player->getPosition());
    ASSERT_EQ(player->getPosition(),QPoint(0,240));


     player->keyPressEvent(eventmovedown);
     ASSERT_EQ(player->getPosition(),QPoint(0,248));


     delete eventmovedown;


}
TEST_F(PacmanTestsMockedGame,MovePosibilityChecksAllMovesMoveRight)
{


    QKeyEvent * eventright = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Right,Qt::NoModifier);

    EXPECT_CALL(m_board,checkIfCrossroad(_)).WillRepeatedly(Return(true));
    QVector<QPoint> AllSidesCrossroadVector{QPoint(0,240),QPoint(-24,240), QPoint(24,240),QPoint(0,200),QPoint(0,280)};
    EXPECT_CALL(m_board,getMoveVector(_)).WillRepeatedly(Return(AllSidesCrossroadVector));
    player->movePosibilityCheck(player->getPosition());
    ASSERT_EQ(player->getPosition(),QPoint(0,240));


     player->keyPressEvent(eventright);
     ASSERT_EQ(player->getPosition(),QPoint(8,240));






     delete eventright;

}
TEST_F(PacmanTestsMockedGame,MovePosibilityChecksAllMovesMoveLeft)
{

    QKeyEvent * eventmoveleft = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Left,Qt::NoModifier);

    EXPECT_CALL(m_board,checkIfCrossroad(_)).WillRepeatedly(Return(true));
    QVector<QPoint> AllSidesCrossroadVector{QPoint(0,240),QPoint(-24,240), QPoint(24,240),QPoint(0,200),QPoint(0,280)};
    EXPECT_CALL(m_board,getMoveVector(_)).WillRepeatedly(Return(AllSidesCrossroadVector));
    player->movePosibilityCheck(player->getPosition());
    ASSERT_EQ(player->getPosition(),QPoint(0,240));

     player->keyPressEvent(eventmoveleft);
     ASSERT_EQ(player->getPosition(),QPoint(-8,240));

     delete eventmoveleft;

}



TEST_F(PacmanTestsMockedGame,PacmanMoveUp)
{

    QKeyEvent * event = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Up,Qt::NoModifier);
    EXPECT_CALL(m_board,checkIfCrossroad(_)).WillRepeatedly(Return(true));
    QVector<QPoint> AllSidesCrossroadVector{QPoint(336,96),QPoint(336,24), QPoint(408,96),QPoint(336,144),QPoint(288,96)};
    EXPECT_CALL(m_board,getMoveVector(_)).WillRepeatedly(Return(AllSidesCrossroadVector));
    player->setPosition(QPoint(336,96));
    player->movePosibilityCheck(QPoint(336,96));
    EXPECT_EQ(player->getPosition(),QPoint(336,96));

    player->keyPressEvent(event);
     EXPECT_EQ(player->getPosition(),QPoint(336,88));
     player->keyPressEvent(event);
     EXPECT_EQ(player->getPosition(),QPoint(336,80));
    delete event;

}




TEST_F(PacmanTestsMockedGame,PacmanMoveDown)
{

    QKeyEvent * event = new QKeyEvent(QEvent::KeyPress,Qt::Key::Key_Down,Qt::NoModifier);
    EXPECT_CALL(m_board,checkIfCrossroad(_)).WillRepeatedly(Return(true));
    QVector<QPoint> AllSidesCrossroadVector{QPoint(336,96),QPoint(336,24), QPoint(408,96),QPoint(336,144),QPoint(288,96)};
    EXPECT_CALL(m_board,getMoveVector(_)).WillRepeatedly(Return(AllSidesCrossroadVector));
    player->setPosition(QPoint(336,96));
    player->movePosibilityCheck(QPoint(336,96));
    EXPECT_EQ(player->getPosition(),QPoint(336,96));

    player->keyPressEvent(event);
     EXPECT_EQ(player->getPosition(),QPoint(336,104));
     player->keyPressEvent(event);
     EXPECT_EQ(player->getPosition(),QPoint(336,112));

    delete event;

}

TEST(CollisionTests,CollidingWithGhostInPowerBallModeSetsGhostToDead)
{

    NiceMock<mock_sysfunc> * m_sys = new  NiceMock<mock_sysfunc>;
    NiceMock<mock_mainmenu> * m_men = new NiceMock<mock_mainmenu>;
    std::shared_ptr<mock_game> game = std::make_shared<mock_game>(m_sys,m_men);
    ghost * Ghost1 = new ghost(game.get(),0);
    pacman * player=new pacman(game.get());
    QList<QGraphicsItem*> Ghosts;
    Ghosts.push_front(Ghost1);
    EXPECT_CALL(*game,isPowerBallMode()).WillRepeatedly(Return(true));
    player->CollisionReact(Ghosts);
    EXPECT_TRUE(Ghost1->isDead());

    delete Ghost1;
    delete player;
}

TEST(CollisionTests,CollidingWithGhostNormalModeSetsPacmanDead)
{

    NiceMock<mock_sysfunc> * m_sys = new  NiceMock<mock_sysfunc>;
    NiceMock<mock_mainmenu> * m_men = new NiceMock<mock_mainmenu>;
    std::shared_ptr<mock_game> game = std::make_shared<mock_game>(m_sys,m_men);
    ghost * Ghost1 = new ghost(game.get(),0);
    pacman * player=new pacman(game.get());
    QList<QGraphicsItem*> Ghosts;
    Ghosts.push_front(Ghost1);
    EXPECT_CALL(*game,isPowerBallMode()).WillRepeatedly(Return(false));
    EXPECT_CALL(*game,decreaseHealth()).Times(2);
    player->CollisionReact(Ghosts);
    EXPECT_FALSE(Ghost1->isDead());
    EXPECT_EQ(player->getPosition(),QPoint(360,240));
    player->CollisionReact(Ghosts);
    EXPECT_EQ(player->getPosition(),QPoint(0,240));


    delete Ghost1;
    delete player;

}

TEST(CollisionTests,CollidingWithBallIncereasesScore)
{
    NiceMock<mock_sysfunc> * m_sys = new  NiceMock<mock_sysfunc>;
    NiceMock<mock_mainmenu> * m_men = new NiceMock<mock_mainmenu>;
    std::shared_ptr<mock_game> game = std::make_shared<mock_game>(m_sys,m_men);
    PointBall * ball = new PointBall(QPoint(0,0));
    pacman * player=new pacman(game.get());
    QList<QGraphicsItem *> balls {ball};
    EXPECT_CALL(*game,increaseScore());

    player->CollisionReact(balls);


    delete player;
}

TEST(CollisionTests,CollidingWithPowerBallstartsPowerballMode)
{

    NiceMock<mock_sysfunc> * m_sys = new  NiceMock<mock_sysfunc>;
    NiceMock<mock_mainmenu> * m_men = new NiceMock<mock_mainmenu>;
    std::shared_ptr<mock_game> game = std::make_shared<mock_game>(m_sys,m_men);
    Hyperball * ball = new Hyperball(QPoint(0,0));
    pacman * player=new pacman(game.get());
    QList<QGraphicsItem *> balls;
    balls.push_front(ball);
    EXPECT_CALL(*game,PowerBallActivation());
    player->CollisionReact(balls);
    delete player;
}

TEST(BoardTests, AllCrossroadsCreated)
{

    PacmanBoard * board = new PacmanBoard();

    auto result = board->getAllCrossroads();
    EXPECT_EQ(result.size(),68);
    delete board;



}



TEST(BoardTests, CheckingIfCrossroad)
{

    PacmanBoard * board = new PacmanBoard();
    QPoint ShouldBeCrossroad1(144,312);
    QPoint ShouldBeCrossroad2(240,360);
    QPoint ShouldBeCrossroad3(24,336);
    QPoint NotCrossroad1(13,13);
    QPoint NotCrossroad2(183,255);
    QPoint NotCrossroad3(411,287);



    EXPECT_TRUE(board->checkIfCrossroad(ShouldBeCrossroad1));
    EXPECT_TRUE(board->checkIfCrossroad(ShouldBeCrossroad2));
    EXPECT_TRUE(board->checkIfCrossroad(ShouldBeCrossroad3));
    EXPECT_FALSE(board->checkIfCrossroad(NotCrossroad1));
    EXPECT_FALSE(board->checkIfCrossroad(NotCrossroad2));
    EXPECT_FALSE(board->checkIfCrossroad(NotCrossroad3));


    delete board;



}
TEST(BoardTests, MovePosibilities)
{

    PacmanBoard * board = new PacmanBoard();

    QVector<QPoint>ExpectedResoult1{QPoint(336,408),QPoint(336,432), QPoint(288,408),QPoint(336,360)};
    QVector<QPoint>ExpectedResoult2{QPoint(240,24),QPoint(240,96), QPoint(336,24)};
    QPoint SearchPoint(336,408);
    QPoint SearchPoint2(240,24);
    EXPECT_EQ(board->getMoveVector(SearchPoint),ExpectedResoult1);
    EXPECT_EQ(board->getMoveVector(SearchPoint2),ExpectedResoult2);
    delete board;



}
TEST(BoardTests, MovePosibilitieNotCrossroadPoint)
{

    PacmanBoard * board = new PacmanBoard();
    QPoint NotCrossroadPoint(1,1);
    EXPECT_EQ(board->getMoveVector(NotCrossroadPoint).size(),0);
    delete board;

}

TEST(BoardTests, AllPointBallsCreated)
{

    PacmanBoard * board = new PacmanBoard();
    EXPECT_EQ(board->getPointBallLocations().size(),190);
    delete board;

}

TEST(BoardTests, AddingNewMove)
{

    PacmanBoard * board = new PacmanBoard();
    QVector<QPoint> CrossroadToAdd{QPoint(0,240),QPoint(-24,240), QPoint(24,240),QPoint(0,200),QPoint(0,280)};
    QPoint NewCrossroad(0,240);
    int OldSize=board->getAllCrossroads().size();
    board->addMoveVector(CrossroadToAdd);
    EXPECT_TRUE(board->checkIfCrossroad(NewCrossroad));
    EXPECT_EQ(OldSize+1,board->getAllCrossroads().size());
    delete board;

}
class GhostTests: public Test
{
protected:
    std::shared_ptr<mock_game> m_game;
    NiceMock<mock_sysfunc> * m_sys;
    NiceMock<mock_mainmenu> * m_men;
    ghost * ghost0;
    ghost * ghost1;
    ghost * ghost2;
    ghost * ghost3;
public:
    GhostTests()
    {
        m_sys=new NiceMock<mock_sysfunc>;
        m_men = new NiceMock<mock_mainmenu>;
        m_game = std::make_shared<mock_game>(m_sys,m_men);
        ghost0 = new ghost(m_game.get(),0);
        ghost1 = new ghost(m_game.get(),1);
        ghost2 = new ghost(m_game.get(),2);
        ghost3 = new ghost(m_game.get(),3);

    }

    ~GhostTests()
    {
        delete ghost0;
        delete ghost1;
        delete ghost2;
        delete ghost3;
        m_game.reset();

    }


};


TEST_F(GhostTests,AfterGoingOutSettingScatteredModeTargetNormalMode)
{

    EXPECT_CALL(*m_game,isPowerBallMode()).WillRepeatedly(Return(false));
    ghost0->GoOut();
    ghost1->GoOut();
    ghost2->GoOut();
    ghost3->GoOut();
    EXPECT_NE(ghost0->getTargetdestination(),QPoint(999,999));
    EXPECT_NE(ghost1->getTargetdestination(),QPoint(999,999));
    EXPECT_NE(ghost2->getTargetdestination(),QPoint(999,999));
    EXPECT_NE(ghost3->getTargetdestination(),QPoint(999,999));


}

TEST_F(GhostTests,CantGoOutInPowerBallMode)
{

    EXPECT_CALL(*m_game,isPowerBallMode()).WillRepeatedly(Return(true));

    ghost0->GoOut();
    ghost1->GoOut();
    ghost2->GoOut();
    ghost3->GoOut();
    EXPECT_EQ(ghost0->getTargetdestination(),QPoint(999,999));
    EXPECT_EQ(ghost1->getTargetdestination(),QPoint(999,999));
    EXPECT_EQ(ghost2->getTargetdestination(),QPoint(999,999));
    EXPECT_EQ(ghost3->getTargetdestination(),QPoint(999,999));


}


TEST_F(GhostTests,ScatteredModeGoodCorners)
{

    EXPECT_CALL(*m_game,isPowerBallMode()).WillRepeatedly(Return(false));

    ghost0->GoOut();
    ghost1->GoOut();
    ghost2->GoOut();
    ghost3->GoOut();
    auto targetofghost0 = ghost0->getTargetdestination();
    auto targetofghost1 = ghost1->getTargetdestination();
    auto targetofghost2 = ghost2->getTargetdestination();
    auto targetofghost3 = ghost3->getTargetdestination();
    EXPECT_TRUE(targetofghost0.x()<216 && targetofghost0.y()<240 &&targetofghost0.x()>=0 && targetofghost0.y()>=0);
    EXPECT_TRUE(targetofghost1.x()>=216 && targetofghost1.y()<240 &&targetofghost1.x()<432 && targetofghost1.y()>=0);
    EXPECT_TRUE(targetofghost2.x()<216 && targetofghost2.y()>240 &&targetofghost2.x()>=0 && targetofghost2.y()<=480);
    EXPECT_TRUE(targetofghost3.x()>=216 && targetofghost3.y()>240 &&targetofghost3.x()<432 && targetofghost3.y()<=480);

}



TEST_F(GhostTests,ChaseModeDirectAttack)
{

    EXPECT_CALL(*m_game,getPacmanPosition()).WillOnce(Return(QPoint(32,32))).WillRepeatedly(Return(QPoint(50,50)));
    ghost0->ChaseModeDirectAttack();
    EXPECT_EQ(ghost0->getTargetdestination(),QPoint(32,32));
    ghost0->ChaseModeDirectAttack();
    EXPECT_EQ(ghost0->getTargetdestination(),QPoint(50,50));

}

TEST_F(GhostTests,ChaseModeAttackFromBehind)
{
    EXPECT_CALL(*m_game,getPacmanLastMove()).WillOnce(Return(QPoint(99,99))).WillRepeatedly(Return(QPoint(155,155)));
    ghost0->ChaseModeAttackFromBehind();
    EXPECT_EQ(ghost0->getTargetdestination(),QPoint(99,99));
    ghost0->ChaseModeAttackFromBehind();
    EXPECT_EQ(ghost0->getTargetdestination(),QPoint(155,155));

}

TEST_F(GhostTests,ChaseModeAttackPredictedPosition)
{

    EXPECT_CALL(*m_game,getPacmanPosition()).WillOnce(Return(QPoint(32,32))).WillRepeatedly(Return(QPoint(50,50)));
    EXPECT_CALL(*m_game,getPacmanDirection()).WillOnce(Return(0)).WillOnce(Return(1)).WillOnce(Return(2)).WillOnce(Return(3));

    ghost2->ChaseModeAttackPredictedPosition();
    EXPECT_EQ(ghost2->getTargetdestination(),QPoint(132,32));
    ghost2->ChaseModeAttackPredictedPosition();
    EXPECT_EQ(ghost2->getTargetdestination(),QPoint(50,150));
    ghost2->ChaseModeAttackPredictedPosition();
    EXPECT_EQ(ghost2->getTargetdestination(),QPoint(-50,50));
    ghost2->ChaseModeAttackPredictedPosition();
    EXPECT_EQ(ghost2->getTargetdestination(),QPoint(50,-50));
}

TEST_F(GhostTests,ChaseModeGetThenRunAwayTactics)
{

    EXPECT_CALL(*m_game,getPacmanPosition()).WillRepeatedly(Return(QPoint(50,20)));
    ghost3->setPos(QPoint(432,432));
    ghost3->ChaseModeGetThenRunAwayTactics();
    EXPECT_EQ(ghost3->getTargetdestination(),QPoint(50,20));
    ghost3->setPos(QPoint(60,30));
    ghost3->ChaseModeGetThenRunAwayTactics();
    EXPECT_EQ(ghost3->getTargetdestination(),QPoint(382,484));

}
TEST_F(GhostTests,PixMapValnurableMode)
{
 ghost0->setValnurableMode();
 ghost0->animate();
 QPixmap ExpectedPixmap(":/img/img/ghostvulnerable.png");
 EXPECT_EQ(ghost0->pixmap(),ExpectedPixmap);
}
TEST_F(GhostTests,AfterDeathGhostIsGoingToCage)
{
 ghost0->setDead();
 EXPECT_EQ(ghost0->getTargetdestination(),QPoint(216,240));
}


TEST(GhostClassTests,ResurectionAfterComingToCage)
{

    mock_ghost * ghost0 = new mock_ghost();
    mock_ghost * ghost1 = new mock_ghost();
    mock_ghost * ghost2 = new mock_ghost();
    mock_ghost * ghost3 = new mock_ghost();
    QVector<GhostInterface*> m_ghost_vector {ghost0,ghost1,ghost2,ghost3};
    mock_pacman * m_pacman=new mock_pacman();
    NiceMock<mock_pacmanboard> m_board ;
    NiceMock<mock_sysfunc> m_sys;
    NiceMock<mock_mainmenu> m_men;
    Game * game = new Game(m_ghost_vector,m_pacman,&m_board,&m_sys,&m_men);
 ghost * testghost = new ghost(game,0);
 testghost->setPos(QPoint(96,24));
 testghost->setDead();
 EXPECT_TRUE(testghost->isDead());
 EXPECT_CALL(m_board,checkIfCrossroad(_)).WillRepeatedly(Return(true));
 testghost->setPos(QPoint(192,216));
 testghost->update();
 testghost->setPos(QPoint(216,240));
 testghost->update();
 EXPECT_FALSE(testghost->isDead());
 delete ghost0;
 delete ghost1;
 delete ghost2;
 delete ghost3;
 delete m_pacman;
 delete testghost;



}



TEST_F(GhostTests,ScatterAndChaseModeChanging)
{
    EXPECT_CALL(*m_game,isPowerBallMode()).WillRepeatedly(Return(false));
    EXPECT_CALL(*m_game,getPacmanPosition()).Times(AtLeast(1));
    EXPECT_CALL(*m_game,getPacmanDirection()).Times(AtLeast(1));
    EXPECT_CALL(*m_game,getPacmanLastMove()).Times(AtLeast(1));

    ghost0->GoOut();
    ghost1->GoOut();
    ghost2->GoOut();
    ghost3->GoOut();
    ghost0->setChaseModeTarget();
    ghost0->setChaseModeTarget();
    ghost0->setChaseModeTarget();
    ghost1->setChaseModeTarget();
    ghost1->setChaseModeTarget();
    ghost1->setChaseModeTarget();
    ghost2->setChaseModeTarget();
    ghost2->setChaseModeTarget();
    ghost2->setChaseModeTarget();
    ghost3->setChaseModeTarget();
    ghost3->setChaseModeTarget();
    ghost3->setChaseModeTarget();
    ghost0->setChaseModeTarget();
    ghost1->setChaseModeTarget();
    ghost2->setChaseModeTarget();
    ghost3->setChaseModeTarget();
    auto targetofghost0 = ghost0->getTargetdestination();
    auto targetofghost1 = ghost1->getTargetdestination();
    auto targetofghost2 = ghost2->getTargetdestination();
    auto targetofghost3 = ghost3->getTargetdestination();
    EXPECT_TRUE(targetofghost0.x()<216 && targetofghost0.y()<240 &&targetofghost0.x()>=0 && targetofghost0.y()>=0);
    EXPECT_TRUE(targetofghost1.x()>=216 && targetofghost1.y()<240 &&targetofghost1.x()<432 && targetofghost1.y()>=0);
    EXPECT_TRUE(targetofghost2.x()<216 && targetofghost2.y()>240 &&targetofghost2.x()>=0 && targetofghost2.y()<=480);
    EXPECT_TRUE(targetofghost3.x()>=216 && targetofghost3.y()>240 &&targetofghost3.x()<432 && targetofghost3.y()<=480);
}


TEST(GhostClassTests,Moving)
{

        mock_ghost * ghost0 = new mock_ghost();
        mock_ghost * ghost1 = new mock_ghost();
        mock_ghost * ghost2 = new mock_ghost();
        mock_ghost * ghost3 = new mock_ghost();
        QVector<GhostInterface*> m_ghost_vector {ghost0,ghost1,ghost2,ghost3};
        mock_pacman * m_pacman=new mock_pacman();
        NiceMock<mock_pacmanboard> m_board ;
        NiceMock<mock_sysfunc> m_sys;
        NiceMock<mock_mainmenu> m_men;
        Game * game = new Game(m_ghost_vector,m_pacman,&m_board,&m_sys,&m_men);

        QPoint Target1 = QPoint(264,336);
        QPoint Target2 = QPoint(96,0);
        QPoint Target3 = QPoint(440,96);
        QPoint Target4 = QPoint(200,96);
        auto VectorOfMoves1 = QVector<QPoint>{QPoint(336,96),QPoint(336,24), QPoint(408,96),QPoint(336,144),QPoint(288,96)};
        auto VectorOfMoves2 = QVector<QPoint>{QPoint(96,96),QPoint(96,24), QPoint(24,96),QPoint(144,96),QPoint(96,144)};

       ghost * testghost = new ghost(game,0);
       testghost->GoOut();

       testghost->setPos(QPoint(336,96));

       EXPECT_CALL(m_board,getMoveVector(_)).WillOnce(Return(VectorOfMoves1)).WillOnce(Return(VectorOfMoves2)).WillRepeatedly(Return(VectorOfMoves1));

       testghost->setTargetdestination(Target1);
       testghost->ChooseNextMove();
       testghost->setVelocity((QPoint(QPoint(336,96))));
       testghost->update();
       EXPECT_EQ(testghost->pos(),QPointF(336,98));
       testghost->setPos(QPoint(96,96));
       testghost->setTargetdestination(Target2);
       testghost->ChooseNextMove();
       testghost->setVelocity((QPoint(96,96)));
       testghost->update();
       EXPECT_EQ(testghost->pos(),QPointF(96,94));
       testghost->update();
       EXPECT_EQ(testghost->pos(),QPointF(96,92));
       testghost->setPos(QPoint(336,96));
       testghost->setTargetdestination(Target3);
       testghost->ChooseNextMove();
       testghost->setVelocity((QPoint(336,96)));
       testghost->update();
       EXPECT_EQ(testghost->pos(),QPointF(338,96));
       testghost->setPos(QPoint(336,96));
       testghost->setTargetdestination(Target4);
       testghost->ChooseNextMove();
       testghost->setVelocity((QPoint(336,96)));
       testghost->update();
       EXPECT_EQ(testghost->pos(),QPointF(334,96));


       delete ghost0;
       delete ghost1;
       delete ghost2;
       delete ghost3;
       delete m_pacman;
       delete testghost;
}

TEST(GhostClassTests,ChoosingNextMove)
{

        mock_ghost * ghost0 = new mock_ghost();
        mock_ghost * ghost1 = new mock_ghost();
        mock_ghost * ghost2 = new mock_ghost();
        mock_ghost * ghost3 = new mock_ghost();
        QVector<GhostInterface*> m_ghost_vector {ghost0,ghost1,ghost2,ghost3};
        mock_pacman * m_pacman=new mock_pacman();
        NiceMock<mock_pacmanboard> m_board ;
        NiceMock<mock_sysfunc> m_sys;
        NiceMock<mock_mainmenu> m_men;
        Game * game = new Game(m_ghost_vector,m_pacman,&m_board,&m_sys,&m_men);

        QPoint Target1 = QPoint(264,336);
        QPoint Target2 = QPoint(96,0);
        auto VectorOfMoves1 = QVector<QPoint>{QPoint(336,96),QPoint(336,24), QPoint(408,96),QPoint(336,144),QPoint(288,96)};
        auto VectorOfMoves2 = QVector<QPoint>{QPoint(96,96),QPoint(96,24), QPoint(24,96),QPoint(144,96),QPoint(96,144)};

       ghost * testghost = new ghost(game,0);

       testghost->GoOut();

       testghost->setPos(QPoint(336,96));

       EXPECT_CALL(m_board,getMoveVector(_)).WillOnce(Return(VectorOfMoves1)).WillOnce(Return(VectorOfMoves2));

       testghost->setTargetdestination(Target1);

       testghost->ChooseNextMove();
       EXPECT_EQ(testghost->getNextMove(),QPoint(336,144));
       testghost->setPos(QPoint(96,96));
       testghost->setTargetdestination(Target2);
       testghost->ChooseNextMove();
       EXPECT_EQ(testghost->getNextMove(),QPoint(96,24));

       delete ghost0;
       delete ghost1;
       delete ghost2;
       delete ghost3;
       delete m_pacman;
       delete testghost;
}

TEST(GhostClassTests,Animating)
{


    mock_ghost * ghost0 = new mock_ghost();
    mock_ghost * ghost1 = new mock_ghost();
    mock_ghost * ghost2 = new mock_ghost();
    mock_ghost * ghost3 = new mock_ghost();
    QVector<GhostInterface*> m_ghost_vector {ghost0,ghost1,ghost2,ghost3};
    mock_pacman * m_pacman=new mock_pacman();
    NiceMock<mock_pacmanboard> m_board ;
    NiceMock<mock_sysfunc> m_sys;
    NiceMock<mock_mainmenu> m_men;
    Game * game = new Game(m_ghost_vector,m_pacman,&m_board,&m_sys,&m_men);

    QPoint Target1 = QPoint(264,336);
    QPoint Target2 = QPoint(96,0);
    QPoint Target3 = QPoint(440,96);
    QPoint Target4 = QPoint(200,96);
    auto VectorOfMoves1 = QVector<QPoint>{QPoint(336,96),QPoint(336,24), QPoint(408,96),QPoint(336,144),QPoint(288,96)};
    auto VectorOfMoves2 = QVector<QPoint>{QPoint(96,96),QPoint(96,24), QPoint(24,96),QPoint(144,96),QPoint(96,144)};
    auto right=QPixmap(":/img/img/ghostright.png");
    auto up=QPixmap(":/img/img/ghostup.png");
    auto down=QPixmap(":/img/img/ghostdown.png");
    auto left=QPixmap(":/img/img/ghostleft.png");
   ghost * testghost = new ghost(game,0);
   testghost->GoOut();

   testghost->setPos(QPoint(336,96));

   EXPECT_CALL(m_board,getMoveVector(_)).WillOnce(Return(VectorOfMoves1)).WillOnce(Return(VectorOfMoves2)).WillRepeatedly(Return(VectorOfMoves1));

   testghost->setTargetdestination(Target1);
   testghost->ChooseNextMove();
   testghost->setVelocity((QPoint(QPoint(336,96))));
   testghost->animate();
   EXPECT_EQ(testghost->pixmap(),down);
   testghost->setPos(QPoint(96,96));
   testghost->setTargetdestination(Target2);
   testghost->ChooseNextMove();
   testghost->setVelocity((QPoint(96,96)));
   testghost->animate();
   EXPECT_EQ(testghost->pixmap(),up);
   testghost->setPos(QPoint(336,96));
   testghost->setTargetdestination(Target3);
   testghost->ChooseNextMove();
   testghost->setVelocity((QPoint(336,96)));
   testghost->animate();
   EXPECT_EQ(testghost->pixmap(),right);
   testghost->setPos(QPoint(336,96));
   testghost->setTargetdestination(Target4);
   testghost->ChooseNextMove();
   testghost->setVelocity((QPoint(336,96)));
   testghost->animate();
   EXPECT_EQ(testghost->pixmap(),left);


   delete ghost0;
   delete ghost1;
   delete ghost2;
   delete ghost3;
   delete m_pacman;
   delete testghost;
}
