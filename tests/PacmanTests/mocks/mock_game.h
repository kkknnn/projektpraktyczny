#ifndef MOCK_GAME_H
#define MOCK_GAME_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <QGraphicsItem>
#include <QGraphicsView>
#include "../../inc/game.h"
#include "../../inc/ghost.h"
#include "mock_sysfunc.h"
#include "mock_mainmenu.h"


class mock_game: public Game
{

    Q_OBJECT

public:
    mock_game(mock_sysfunc * a, mock_mainmenu * b): Game (b,a)
    {

    }
    MOCK_METHOD0(getPacmanPosition,QPoint()); //
    MOCK_METHOD1(addObject, bool(QGraphicsItem *)); //
    MOCK_METHOD0(getPacmanLastMove,QPoint());  //
    MOCK_METHOD0(getPacmanDirection,int()); //
    MOCK_METHOD0(isPowerBallMode,bool()); //
    MOCK_CONST_METHOD0(getScore,int()); //
    MOCK_CONST_METHOD0(getHealth,int()); //
    MOCK_CONST_METHOD0(getAllGhosts,QVector<GhostInterface *>()); //
    MOCK_METHOD0(decreaseHealth,void());//
    MOCK_METHOD0(increaseScore,void()); //
    MOCK_METHOD0(GhostRelease,void()); //
    MOCK_METHOD0(UpdateAllObjects,void());
    MOCK_METHOD0(PowerBallActivation,void());
};

#endif // MOCK_GAME_H

                                        //Game(QWidget * parent=nullptr);
                                        //bool addObject(QGraphicsItem *);
                                        //PacmanBoard * board;
                                        //QPoint getPacmanPosition();
                                        //QPoint getPacmanLastMove();
                                        //int getPacmanDirection();
                                        //bool isPowerBallMode();
                                        //~Game();

                                        //int getScore() const;
                                        //void decreaseHealth();
                                        //void increaseScore();
                                        //int getHealth() const;

                                        //public slots:
                                        //void GhostRelease();
                                        //void UpdateAllObjects();
                                        //void PowerBallActivation();

                                        //private:
                                        //pacman * player;
                                        //int GoOutSequencer;
                                        //QVector<ghost*> GhostArray;
                                        //QGraphicsScene * PacmanGame;
                                        //QTimer * GhostMoveTimer;
                                        //QTimer * GhostMoveOut;
                                        //QTimer * PowerballMode;
                                        //QLabel * ScoreLabel;
                                        //QLabel * LifeLabel;
                                        //int Score;
                                        //int Health;
                                        //bool powerballmode;
