#ifndef MOCK_PACKMAN_H
#define MOCK_PACKMAN_H


#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../../inc/pacman.h"
#include "../../inc/game.h"




class mock_pacman: public PacmanInterface
{
public:
    virtual ~mock_pacman()=default;
    MOCK_METHOD1(keyPressEvent,void(QKeyEvent *));
    MOCK_METHOD0(getPosition,QPoint());
    MOCK_METHOD1(setPosition,void(QPoint));
    MOCK_METHOD0(update,void());
    MOCK_CONST_METHOD0(getAnimationPhase,int());
    MOCK_METHOD1(setAnimationPhase,void(int));
    MOCK_METHOD0(animate,void());
    MOCK_CONST_METHOD0(getActualDirection,int());
    MOCK_CONST_METHOD0(getLastMove,QPoint());
    MOCK_METHOD1(movePosibilityCheck,void(QPoint));
    MOCK_METHOD1(CollisionReact,void(QList <QGraphicsItem*> &));
};


#endif // MOCK_PACKMAN_H
