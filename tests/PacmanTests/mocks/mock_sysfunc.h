#ifndef MOCK_SYSFUNC_H
#define MOCK_SYSFUNC_H
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../../inc/sysfunctions.h"

class mock_sysfunc: public SysFuctionInterface
{
public:
    MOCK_METHOD1(showView,void(QGraphicsView *));
    MOCK_METHOD1(ReadBestPlayers,QList<QPair<QString,int>>(const QString &));
    MOCK_METHOD2(SaveBestPlayers,void(QList<QPair<QString,int>>&, const QString &));
    MOCK_METHOD1(initFinalMessage,void(QGraphicsView *));
    MOCK_METHOD0(setFinalMessageWin, void());
    MOCK_METHOD0(setFinalMessageLoose,void());
    MOCK_CONST_METHOD0(showFinalMessage,void());
    MOCK_CONST_METHOD0(GameInit,void());
    MOCK_CONST_METHOD0(MenuInit,void());

};

#endif // MOCK_SYSFUNC_H
//virtual void GameInit() const
