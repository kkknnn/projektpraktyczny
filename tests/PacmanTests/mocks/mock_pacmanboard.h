#ifndef MOCK_PACMANBOARD_H
#define MOCK_PACMANBOARD_H




#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../../inc/board.h"

class mock_pacmanboard: public BoardInterface
{

public:
    MOCK_METHOD1(getMoveVector,QVector<QPoint>(QPoint &));
    MOCK_METHOD1(addMoveVector,void(QVector<QPoint> &));
    MOCK_METHOD0(getAllCrossroads,QVector<QPoint>());
    MOCK_METHOD1(checkIfCrossroad,bool(QPoint &));
    MOCK_CONST_METHOD0(getPointBallLocations,QVector<QPoint>());
    MOCK_CONST_METHOD1(getPointballSingleLocation,QPoint(int));

};
#endif //
