#ifndef MOCK_GHOST_H
#define MOCK_GHOST_H


#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../../inc/ghost.h"

class mock_ghost: public GhostInterface
{
public:

    MOCK_METHOD0(animate,void());
    MOCK_CONST_METHOD0(getTargetdestination,QPoint());
    MOCK_METHOD1(setTargetdestination,void(const QPoint &));
    MOCK_METHOD1(setVelocity,void(const QPoint &));
    MOCK_METHOD0(ChooseNextMove,void());
    MOCK_METHOD0(isWaiting,bool());
    MOCK_METHOD0(setDead,void());
    MOCK_METHOD0(update,void());
    MOCK_METHOD0(setScatteredModeTarget,void());
    MOCK_METHOD0(setChaseModeTarget,void());
    MOCK_METHOD0(setValnurableMode,void());
    MOCK_METHOD0(GoOut,void());
    MOCK_METHOD0(ChaseModeDirectAttack,void());
    MOCK_METHOD0(ChaseModeAttackFromBehind,void());
    MOCK_METHOD0(ChaseModeAttackPredictedPosition, void());
    MOCK_METHOD0(ChaseModeGetThenRunAwayTactics,void());
    MOCK_CONST_METHOD0(isDead,bool());
};



#endif
