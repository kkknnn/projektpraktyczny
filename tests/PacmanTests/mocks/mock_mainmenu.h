#ifndef MOCK_MAINMENU_H
#define MOCK_MAINMENU_H
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../../inc/mainmenu.h"
#include "../../inc/sysfunctions.h"

class mock_mainmenu: public MainMenuInterface
{

public:
   // MOCK_METHOD0(LoadBestPlayerList,void());
    MOCK_CONST_METHOD0(getTopPlayers,QList<QPair<QString,int>>());
    MOCK_CONST_METHOD0(getPlayerNickname,QString());
    MOCK_METHOD0(GameInit,void());


};

#endif // MOCK_MAINMENU_H
