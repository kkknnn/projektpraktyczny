#include "tst_alltests.h"

#include <gtest/gtest.h>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc,argv);
    testing::InitGoogleMock(&argc, argv);
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
