include(gtest_dependency.pri)
QT -=gui

QT += multimedia \
      core\
      widgets\
      testlib


TEMPLATE = app
CONFIG += console c++11
CONFIG += thread
CONFIG += c++14

QMAKE_CXXFLAGS += --coverage
QMAKE_LFLAGS += --coverage

LIBS += \
-lgcov

HEADERS += \
        tst_alltests.h \
    ../../inc/ball.h \
    ../../inc/board.h \
    ../../inc/game.h \
    ../../inc/ghost.h \
    ../../inc/hyperball.h \
    ../../inc/mainmenu.h \
    ../../inc/pacman.h \
    mocks/mock_game.h \
    mocks/mock_packman.h \
    mocks/mock_pacmanboard.h \
    mocks/mock_ghost.h \
    ../../inc/viewfactory.h \
    mocks/mock_sysfunc.h \
    mocks/mock_mainmenu.h \
    ../../inc/sysfunctions.h \
    ../../inc/updatableoncollision.h

SOURCES += \
        main.cpp \
    ../../src/ball.cpp \
    ../../src/board.cpp \
    ../../src/game.cpp \
    ../../src/ghost.cpp \
    ../../src/hyperball.cpp \
    ../../src/mainmenu.cpp \
    ../../src/pacman.cpp \
    ../../src/viewfactory.cpp \
    ../../src/sysfunctions.cpp

RESOURCES += \
    ../../res.qrc
